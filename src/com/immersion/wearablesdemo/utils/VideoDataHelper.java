package com.immersion.wearablesdemo.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.Bitmap;

import com.immersion.wearablesdemo.activities.ScreenSlideActivity;
import com.immersion.wearablesdemo.models.VideoData;

/**
 * A currently non-thread safe helper class to load a single instance of all the {@link VideoData} models into a {@link Map}
 * of {@link List}s.  Each list is indexed by a corresponding index [0, PAGE_COUNT) of a tab on the apps home screen.  Currently, per the 
 * requirements of the app, there must be PAGE_COUNT pages and each page must contain at least two videos and at most three.  Thus, there
 * will be PAGE_COUNT non-null lists with 2 to 3 elements in each of them.
 * 
 * @author ivan stashak
 *
 */
public final class VideoDataHelper
{

	/**
	 * This variable must be the same as the intended tabs on the home screen.
	 */
	public static final int PAGE_COUNT = ScreenSlideActivity.NUM_PAGES;
	public static final int MAX_VIDEOS_PER_PAGE = 3;
	
	private static Map<String, Bitmap> imageCache = new HashMap<String, Bitmap>(PAGE_COUNT * 4);
	
	private Map<Integer, List<VideoData>> mVideoData = new HashMap<Integer, List<VideoData>>(PAGE_COUNT);
	
	
	private VideoDataHelper(final Context context)
	{
		for(int i = 0; i < PAGE_COUNT; ++i)
		{
			final List<VideoData> pageVideoData = new ArrayList<VideoData>(MAX_VIDEOS_PER_PAGE);
			mVideoData.put(i, pageVideoData);
			
			for(int j = 0; j < MAX_VIDEOS_PER_PAGE; ++j)
			{
				final VideoData videoData = buildVideoDataObject(context, i, j);
				
				/*
				if(videoData.getMovieImage() == null)
				{
					break;
				}
				*/
				
				
				final Bitmap videoBitmap = FileUtils.getBitmapFromAssetFile(context, videoData.getVideoBitmapAssetUri());
				
				// Break out of the loop if the image doesn't exist.
				if(videoBitmap == null)
				{
					break;
				}
				
				imageCache.put(videoData.getVideoBitmapAssetUri(), videoBitmap);
				
				if(!imageCache.containsKey(videoData.getIconAssetUri()))
				{
					imageCache.put(videoData.getIconAssetUri(), FileUtils.getBitmapFromAssetFile(context, videoData.getIconAssetUri()));
				}
				
				if(!imageCache.containsKey(videoData.getWhiteIconAssetUri()))
				{
					imageCache.put(videoData.getWhiteIconAssetUri(), FileUtils.getBitmapFromAssetFile(context, videoData.getWhiteIconAssetUri()));
				}
				
				pageVideoData.add(videoData);
			}
		}
	}
	
	private static VideoDataHelper videoDataHelper;
	public static VideoDataHelper getVideoDataHelper(final Context context)
	{
		if(videoDataHelper == null)
		{
			videoDataHelper = new VideoDataHelper(context);
		}
		
		return videoDataHelper;
	}
	
	public static VideoData getVideoData(final Context context, final int pageIndex, final int videoIndex)
	{
		return getPageVideoData(context, pageIndex).get(videoIndex);
	}
	
	public static List<VideoData> getPageVideoData(final Context context, final int pageIndex)
	{
		return getVideoDataHelper(context).mVideoData.get(Integer.valueOf(pageIndex));
	}
	
	public static Bitmap getBitmap(final String assetUri)
	{
		return imageCache.get(assetUri);
	}
	
	public static VideoData getPreviousVideoData(final Context context, final VideoData videoData)
	{	
		final int currentVideoIndex = videoData.getVideoIndex();
		
		// If we know we are not on the first video for the page we can simply return the video proceeding this one on
		// the page.
		if(currentVideoIndex > 0)
		{
			return getVideoData(context, videoData.getPageIndex(), currentVideoIndex - 1);
		}
		
		// Here we know currentVideoIndex == 0, so we need to return the last element of the previous page taking care
		// to move to the last page if we are at the first page.
		final int currentPageIndex = videoData.getPageIndex();
		final int previousPageIndex = currentPageIndex > 0 ? currentPageIndex - 1 : PAGE_COUNT - 1;
		
		final List<VideoData> list = getPageVideoData(context, previousPageIndex);
		
		// Here we know no list is either null nor has zero elements per the app requirements, which makes the logic easier.
		return list.get(list.size() - 1);
	}
	
	public static VideoData getNextVideoData(final Context context, final VideoData videoData)
	{
		final int currentPageIndex = videoData.getPageIndex();
		
		// Get the List of VideoData for the current page.
		final List<VideoData> list = getPageVideoData(context, currentPageIndex);	
		
		final int currentVideoIndex = videoData.getVideoIndex();
		
		// If we are not on the last video for the page return the next one on the page.
		if(currentVideoIndex < list.size() - 1)
		{
			return list.get(currentVideoIndex + 1);
		}
		
		// Here we know that the VideoData passed in is the last one for its particular page, so we need to get the
		// first VideoData object on the next page taking care to move to the first page if we are at the last page.
		return getVideoData(context, (currentPageIndex + 1) % PAGE_COUNT, 0);		
	}
	
	public static VideoData buildVideoDataObject(final Context context, final int pageIndex, final int videoIndex)
	{	
		final VideoData.Builder builder = new VideoData.Builder();
		
		final int tmpPage = pageIndex + 1;
		final int tmpVideo = videoIndex + 1;
	
		final String baseDir = "movie_info/section" + tmpPage;		
		
		builder.setPageIndex(pageIndex)
		.setVideoIndex(videoIndex)
		.setPageTitle(FileUtils.getStringFromAssetFile(context, baseDir + "/section_title.txt"))
		.setPageDescription(FileUtils.getStringFromAssetFile(context, baseDir + "/section_description.txt"))
		.setVideoTitle(FileUtils.getStringFromAssetFile(context, baseDir + "/vignette_title" + tmpVideo + ".txt"));
		
        final String videoFileName = FileUtils.getStringFromAssetFile(context, baseDir + "/video_uri_" + tmpVideo + ".txt");
        builder.setVideoUri(FileUtils.getImmersionDir() + "/" + videoFileName);
        
        final String hapticFileName = FileUtils.getStringFromAssetFile(context, baseDir + "/haptic_uri_" + tmpVideo + ".txt");
        builder.setHapticUri(FileUtils.getImmersionDir() + "/" + hapticFileName)
        
        
        //.setIconBitmap(FileUtils.getBitmapFromAssetFile(context, baseDir + "/icon"+ tmpPage + ".png"))
        .setIconAssetUri(baseDir + "/icon"+ tmpPage + ".png")
        .setWhiteIconAssetUri(baseDir + "/icon"+ tmpPage + "_white.png")
        //.setVideoBitmap(FileUtils.getBitmapFromAssetFile(context, baseDir + "/vignette_icon" + tmpVideo + ".png"));
        .setVideoBitmapAssetUri(baseDir + "/vignette_icon" + tmpVideo + ".png");

        return builder.build();
	}
}

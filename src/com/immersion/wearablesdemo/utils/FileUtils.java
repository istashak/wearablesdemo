package com.immersion.wearablesdemo.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;

import com.immersion.wearablesdemo.R;
import com.immersion.wearablesdemo.models.HapticFileInformation;
import com.immersion.wearablesdemo.utils.FileUtils.FileSpaceStatus;
import com.immersion.wearablesdemo.utils.FileUtils.WriteStatus;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;

/**
 * 
 * @author ivan stashak
 *
 */
public final class FileUtils
{
	
	public static final String TAG = "FileUtils";

    public static final String HAPTIC_STORAGE_FILENAME = "dat.hapt";

    public static String getImmersionDir()
    {
        return Environment.getExternalStorageDirectory() + "/immersion";
    }

    public static boolean fileExists(final String path)
    {
    	return new File(path).exists();
    }
    
    public static boolean createDirectory(final String path)
    {
        final File file = new File(path);

        if (!file.exists())
        {
            return file.mkdirs();
        }

        return true;
    }

    public static File getHapticStorageFile()
    {
        final String immersionDir = getImmersionDir();

        if (createDirectory(immersionDir))
        {
            return new File(immersionDir + "/" + HAPTIC_STORAGE_FILENAME);
        }

        return null;
    }

    public static boolean deleteHapticStorageFile()
    {
        return new File(getImmersionDir() + "/" + HAPTIC_STORAGE_FILENAME)
                .delete();
    }

    public static class WriteStatus
    {
        public WriteStatus()
        {
            success = false;
            bytesWritten = 0l;
        }

        public boolean success;
        public long bytesWritten;
        public Exception exception;
    }

    public static WriteStatus copyAndAppendToFileII(
            final BufferedInputStream from, final BufferedOutputStream to)
    {
        final byte[] buf = new byte[4096];
        final WriteStatus writeStatus = new WriteStatus();

        try
        {
            if (from != null)
            {
                int bytesRead = 0;

                while ((bytesRead = from.read(buf)) >= 0)
                {
                    to.write(buf, 0, bytesRead);
                    writeStatus.bytesWritten += bytesRead;
                }

                writeStatus.success = true;
            }
        } catch (FileNotFoundException e)
        {
            writeStatus.exception = e;
            e.printStackTrace();
        } catch (IOException e)
        {
            writeStatus.exception = e;
            e.printStackTrace();
        }

        return writeStatus;
    }

    public static boolean copyAndAppendToFile(final BufferedInputStream from,
            final BufferedOutputStream to)
    {
        final byte[] buf = new byte[4096];

        try
        {
            if (from != null)
            {
                int bytesRead = 0;

                while ((bytesRead = from.read(buf)) >= 0)
                {
                    to.write(buf, 0, bytesRead);
                }

                return true;
            }
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        return false;
    }
    

	/**
	 * 
	 * @param closeable
	 */
	public static void closeCloseable(final Closeable closeable)
	{
		if(closeable != null)
		{
			try
			{
				closeable.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	
	/**
     * Deletes all empty child directories/folders in the given directory.
     * 
     * @param baseDirectory
     *            The directory from which to delete all empty child
     *            directories.
     */
    public static boolean deleteAllEmptyChildDirectories(
            final String baseDirectory, final boolean deleteBaseDir)
    {
        final File baseDir = new File(baseDirectory);

        if (!baseDir.exists())
        {
            return false;
        }

        final File[] dirs = baseDir.listFiles();
        boolean canDelete = true;

        if (dirs != null)
        {
            final int n = dirs.length;

            for (int i = 0; i < n; ++i)
            {
                Log.v(TAG, "file path = " + dirs[i].getAbsolutePath());

                if (dirs[i].isDirectory())
                {
                    canDelete = deleteAllEmptyChildDirectories(
                            dirs[i].getAbsolutePath(), true);
                } else
                {
                    canDelete = false;
                }
            }
        }

        if (deleteBaseDir && canDelete)
        {
            Log.v(TAG, "deleting: " + baseDir.getAbsolutePath());
            baseDir.delete();
        }

        return canDelete;
    }

    /**
     * Utility function to delete all files under a directory
     * 
     * @param baseDir
     */
    public static void deleteFiles(final String baseDir)
    {
        final File file = new File(baseDir);

        if (file.exists())
        {
            final String deleteCmd = "rm -r " + baseDir;
            final Runtime runtime = Runtime.getRuntime();

            try
            {
                runtime.exec(deleteCmd);
            } catch (IOException e)
            {
            }
        }
    }

    /**
     * Deletes all files under baseDir except the directory child
     * "/baseDir/test"
     * 
     * @param baseDir
     */
    public static void deleteAllChildFiles(final String baseDir)
    {
        final File baseDirFile = new File(baseDir);

        if (baseDirFile.exists() && baseDirFile.isDirectory())
        {
            final File[] files = baseDirFile.listFiles(new FilenameFilter()
            {

                @Override
                public boolean accept(File dir, String filename)
                {

                    return baseDirFile == dir
                            && !filename.equalsIgnoreCase("test");
                }
            });

            final int n = files.length;
            for (int i = 0; i < n; ++i)
            {
                final String fileName = files[i].getAbsolutePath();
                final String deleteCmd = "rm -r " + fileName;
                final Runtime runtime = Runtime.getRuntime();

                try
                {
                    runtime.exec(deleteCmd);
                } catch (IOException e)
                {
                }
            }
        }
    }

    public static class IsDirectoryFileFilter implements FileFilter
    {
        @Override
        public boolean accept(File pathname)
        {

            return pathname.isDirectory();
        }
    }

    public static boolean readStreamToFile(final InputStream is,
            final String absoluteFileName)
    {
        final byte[] buf = new byte[4096];

        BufferedInputStream from = null;
        BufferedOutputStream to = null;

        try
        {
            from = new BufferedInputStream(is);
            to = new BufferedOutputStream(new FileOutputStream(new File(
                    absoluteFileName)));

            int bytesRead = 0;

            while ((bytesRead = from.read(buf)) >= 0)
            {
                to.write(buf, 0, bytesRead);
            }

            return true;
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            closeCloseable(from);
            closeCloseable(to);
        }

        return false;
    }

    public static boolean readStreamToPrivateFile(final Context context,
            final InputStream is, final String relativePath)
    {
        final byte[] buf = new byte[4096];

        BufferedInputStream from = null;
        BufferedOutputStream to = null;

        try
        {
            from = new BufferedInputStream(is);
            to = new BufferedOutputStream(context.openFileOutput(relativePath,
                    Context.MODE_PRIVATE));

            int bytesRead = 0;

            while ((bytesRead = from.read(buf)) >= 0)
            {
                to.write(buf, 0, bytesRead);
            }

            return true;
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            closeCloseable(from);
            closeCloseable(to);
        }

        return false;
    }

    public static boolean logStream(final String tag, final InputStream is)
    {
        final byte[] buf = new byte[4096];

        BufferedInputStream from = null;

        try
        {
            from = new BufferedInputStream(is);

            if (from != null)
            {
                while (from.read(buf) >= 0)
                {
                    Log.d(tag, new String(buf));
                }

                return true;
            }
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            closeCloseable(from);
        }

        return false;
    }

    /**
     * 
     * @param external
     *            whether to return info about the external storage (true) or
     *            internal storage (false)
     * @return The available storage in bytes.
     */
    public static long FreeStorage(boolean external)
    {
        final StatFs statfs = external ? new StatFs(Environment
                .getExternalStorageDirectory().getAbsolutePath()) : new StatFs(
                Environment.getRootDirectory().getAbsolutePath());
        return ((long) statfs.getAvailableBlocks())
                * ((long) statfs.getBlockSize());
    }

    public static FileSpaceStatus doesSpaceExist(
            final long spaceToBeConsumedBeforeFileDownload,
            final long spaceNeededForFile)
    {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED))
        {
            final StatFs stat = new StatFs(Environment
                    .getExternalStorageDirectory().getPath());

            long fileSpaceAvailable = (long) stat.getAvailableBlocks()
                    * (long) stat.getBlockSize();

            if (spaceToBeConsumedBeforeFileDownload + spaceNeededForFile < fileSpaceAvailable)
            {
                return FileSpaceStatus.ENOUGH_SPACE;
            }

            return FileSpaceStatus.NOT_ENOUGH_SPACE;
        }

        return FileSpaceStatus.MEDIA_NOT_MOUNTED;
    }
    
    
    public static HapticFileInformation readHapticFileHeader(final FileChannel fileChannel)
    {
        Log.d(TAG, "************* readHapticFileHeader() beginning *************");
        
        HapticFileInformation hfi = null;
        try
        {
            final ByteBuffer tmp = ByteBuffer.allocate(4);

            tmp.order(ByteOrder.LITTLE_ENDIAN);
            tmp.position(0);

            // Find the size of 'HAPT''fmt ' header info.
            int bytesRead = fileChannel.read(tmp, 0x10);

            if (bytesRead != 4)
            {
                Log.d(TAG, "************* readHapticFileHeader() NOT successful *************");
                return null;
            }

            tmp.flip();
            final int size = tmp.getInt() + 28;

            // Now allocate a buffer big enough to hold all of the header data.
            final ByteBuffer allHeaderInfo = ByteBuffer.allocate(size);
            allHeaderInfo.order(ByteOrder.LITTLE_ENDIAN);

            // Now read the entire header into a buffer, so that we can parse
            // through it.
            bytesRead = fileChannel.read(allHeaderInfo, 0x00);

            if (bytesRead != size)
            {
                Log.d(TAG, "************* readHapticFileHeader() NOT successful *************");
                return null;
            }

            allHeaderInfo.flip();

            // Start parsing the file header
            final HapticFileInformation.Builder b = new HapticFileInformation.Builder();

            b.setFilePath(getHapticStorageFile().getAbsolutePath());

            allHeaderInfo.position(4);
            b.setTotalFileLength(allHeaderInfo.getInt() + 8);

            // jump a head to the pertinent haptic info
            allHeaderInfo.position(0x14);
            b.setMajorVersion(allHeaderInfo.get());

            b.setMinorVersion(allHeaderInfo.get());

            b.setEncoding(allHeaderInfo.get());

            // skip the reserved byte and jump to the sample rate
            allHeaderInfo.position(0x18);
            b.setSampleHertz(allHeaderInfo.getInt());

            final int bls = allHeaderInfo.get();
            final int bhs = allHeaderInfo.get();

            b.setBitsPerSample((bhs << 8) | bls);

            final int numChannels = allHeaderInfo.get();

            b.setNumberOfChannels(numChannels);

            final int[] actuatorArray = new int[numChannels];

            for (int i = 0; i < numChannels; ++i)
            {
                actuatorArray[i] = allHeaderInfo.get();
            }

            b.setActuatorArray(actuatorArray);

            b.setCompressionScheme(allHeaderInfo.get());

            // skip across the 'data' box
            allHeaderInfo.position(allHeaderInfo.position() + 4);
            b.setHapticDataLength(allHeaderInfo.getInt());

            b.setHapticDataStartByteOffset(allHeaderInfo.position());

            hfi = b.build();

            Log.d(TAG, "************* readHapticFileHeader() successful *************");
        }
        catch (IOException e)
        {
            Log.e(TAG, "FileNotFoundException", e);
        }

        return hfi;
    }
    

    public enum FileSpaceStatus
    {
        ENOUGH_SPACE, NOT_ENOUGH_SPACE, MEDIA_NOT_MOUNTED;

        public static FileSpaceStatus valueOfCaseInsensitive(
                final String fileSpaceStatus)
        {
            for (FileSpaceStatus fss : values())
            {
                if (fileSpaceStatus.equalsIgnoreCase(fss.name()))
                {
                    return fss;
                }
            }

            return null;
        }
    }
    
    
	/**
	 * 
	 * @param context
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public static InputStream getInputStreamFromAssetFile(final Context context, final String fileName) throws IOException
	{
		return context.getAssets().open(fileName);
	}
	
	/**
	 * 
	 * @param context
	 * @param fileName
	 * @return
	 */
	public static String getStringFromAssetFile(final Context context, final String fileName)
	{
        InputStream is = null; 
        
        try
        {
        	is = getInputStreamFromAssetFile(context, fileName);
            final int length = is.available();
            final byte[] data = new byte[length];
            is.read(data);
            return new String(data);
        }
        catch (IOException e1)
        {
            e1.printStackTrace();
        }
        finally
        {
        	FileUtils.closeCloseable(is);
        	is = null;
        }
        
        return null;
	}
	
	/**
	 * 
	 * @param context
	 * @param fileName
	 * @return
	 */
	public static Bitmap getBitmapFromAssetFile(final Context context, final String fileName)
	{
		InputStream is = null; 
        
        try
        {
        	is = getInputStreamFromAssetFile(context, fileName);
            
            return BitmapFactory.decodeStream(is);
        }
        catch (IOException e1)
        {
            e1.printStackTrace();
        }
        finally
        {
        	FileUtils.closeCloseable(is);
        	is = null;
        }
        
        return null;
	}
	
	
	
	public static void transferAssetFileToExternalStorage(final Context context, final String fileName)
	{
		try
		{
			final String fullExternalPath = getImmersionDir() + "/" + fileName;
			
			if(!fileExists(fullExternalPath))
			{
				readStreamToFile(getInputStreamFromAssetFile(context, "movies/" + fileName), fullExternalPath);
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void transferAllAssetMoviesToExternalStorage(final Context context)
	{
		
		createDirectory(getImmersionDir());
		
		transferAssetFileToExternalStorage(context, "adidas.mp4");
		transferAssetFileToExternalStorage(context, "adidas.hapt");
		
		transferAssetFileToExternalStorage(context, "doritos.mp4");
		transferAssetFileToExternalStorage(context, "doritos.hapt");
		
		transferAssetFileToExternalStorage(context, "fitbit.mp4");
		transferAssetFileToExternalStorage(context, "fitbit.hapt");
		
		transferAssetFileToExternalStorage(context, "gyunyu.mp4");
		transferAssetFileToExternalStorage(context, "gyunyu.hapt");
		
		transferAssetFileToExternalStorage(context, "jawbone_up.mp4");
		transferAssetFileToExternalStorage(context, "jawbone_up.hapt");
		
		transferAssetFileToExternalStorage(context, "thor.mp4");
		transferAssetFileToExternalStorage(context, "thor.hapt");
	}
}

package com.immersion.wearablesdemo.utils;

import android.os.SystemClock;

/**
 * A simple class for profiling performance.
 * 
 * @author ivan stashak
 *
 */
public class Profiler
{

    public long mStartTime;
    public long mStartTimeII;

    public void startTiming()
    {
        mStartTime = SystemClock.elapsedRealtime();
    }

    public void startTimingII()
    {
        mStartTimeII = SystemClock.elapsedRealtime();
    }

    public long getDuration()
    {
        return SystemClock.elapsedRealtime() - mStartTime;
    }

    public long getDurationII()
    {
        return SystemClock.elapsedRealtime() - mStartTimeII;
    }

}

package com.immersion.wearablesdemo.utils;

/**
 * Created by rbhatia on 9/9/13.
 */

import android.content.Context;
import android.preference.ListPreference;
import android.preference.PreferenceManager;


public class RuntimeInfo extends Object {

    public static boolean areHapticsEnabled(Context context)
    {
        return true;//PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SettingsFragment.KEY_HAPTICS, true);
    }

    public static String getCurrentMode(Context context)
    {
        String mode = "";

        /*
        String applicationMode = PreferenceManager.getDefaultSharedPreferences(context).getString(SettingsFragment.KEY_MODE, "");
        if(applicationMode.equals(context.getString(R.string.mode_eng)))
            mode = context.getString(R.string.eng);
        else if(applicationMode.equals(context.getString(R.string.mode_ux)))
            mode = context.getString(R.string.ux);
        else if(applicationMode.equals(context.getString(R.string.mode_rd)))
            mode = context.getString(R.string.rd);
        else if(applicationMode.equals(context.getString(R.string.mode_qa)))
            mode = context.getString(R.string.qa);
        else if(applicationMode.equals(context.getString(R.string.mode_sales)))
            mode = context.getString(R.string.sales);
		*/
        return mode;
    }

    public static String getCurrentModeShort(Context context)
    {
        String mode = "";
        /*
        String applicationMode = PreferenceManager.getDefaultSharedPreferences(context).getString(SettingsFragment.KEY_MODE, "");
        if(applicationMode.equals(context.getString(R.string.mode_eng)))
            mode = context.getString(R.string.eng_short);
        else if(applicationMode.equals(context.getString(R.string.mode_ux)))
            mode = context.getString(R.string.ux_short);
        else if(applicationMode.equals(context.getString(R.string.mode_rd)))
            mode = context.getString(R.string.rd_short);
        else if(applicationMode.equals(context.getString(R.string.mode_qa)))
            mode = context.getString(R.string.qa_short);
        else if(applicationMode.equals(context.getString(R.string.sales)))
            mode = context.getString(R.string.sales_short);
		*/
        return mode;
    }

    public static String getCurrentServerFromMode(Context context)
    {
        String server = "";

        /*
        String applicationMode = PreferenceManager.getDefaultSharedPreferences(context).getString(SettingsFragment.KEY_MODE, "");
        if(applicationMode.equals(context.getString(R.string.mode_eng)))
            server = context.getString(R.string.eng_server);
        else if(applicationMode.equals(context.getString(R.string.mode_ux)))
            server = context.getString(R.string.ux_server);
        else if(applicationMode.equals(context.getString(R.string.mode_rd)))
            server = context.getString(R.string.rd_server);
        else if(applicationMode.equals(context.getString(R.string.mode_qa)))
            server = context.getString(R.string.qa_server);
        else if(applicationMode.equals(context.getString(R.string.mode_sales)))
            server = context.getString(R.string.sales_server);
		*/
        
        return server;
    }

    public static String getCurrentBucketFromMode(Context context)
    {
        String bucket = "";

        /*
        String applicationMode = PreferenceManager.getDefaultSharedPreferences(context).getString(SettingsFragment.KEY_MODE, "");
        if(applicationMode.equals(context.getString(R.string.mode_eng)))
            bucket = context.getString(R.string.eng_bucket);
        else if(applicationMode.equals(context.getString(R.string.mode_ux)))
            bucket = context.getString(R.string.ux_bucket);
        else if(applicationMode.equals(context.getString(R.string.mode_rd)))
            bucket = context.getString(R.string.rd_bucket);
        else if(applicationMode.equals(context.getString(R.string.mode_qa)))
            bucket = context.getString(R.string.qa_bucket);
        else if(applicationMode.equals(context.getString(R.string.mode_sales)))
            bucket = context.getString(R.string.sales_bucket);
		*/
        
        return bucket;
    }

    public static void setSummaryFromMode(Context context, ListPreference lp)
    {
        String summary = "";

        /*
        String applicationMode = PreferenceManager.getDefaultSharedPreferences(context).getString(SettingsFragment.KEY_MODE, "");
        if(applicationMode.equals(context.getString(R.string.mode_eng)))
            summary = context.getString(R.string.eng);
        else if(applicationMode.equals(context.getString(R.string.mode_ux)))
            summary = context.getString(R.string.ux);
        else if(applicationMode.equals(context.getString(R.string.mode_rd)))
            summary = context.getString(R.string.rd);
        else if(applicationMode.equals(context.getString(R.string.mode_qa)))
            summary = context.getString(R.string.qa);
        else if(applicationMode.equals(context.getString(R.string.mode_sales)))
            summary = context.getString(R.string.sales);
		*/
        
        lp.setSummary(summary);
    }
}

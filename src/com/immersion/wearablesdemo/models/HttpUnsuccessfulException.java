package com.immersion.wearablesdemo.models;

public class HttpUnsuccessfulException extends Exception
{
    private int mHttpStatusCode;

    /**
     * 
     */
    private static final long serialVersionUID = -251711421440827767L;
    
    public HttpUnsuccessfulException(final int httpStatusCode, final String message)
    {
        super(message);
        mHttpStatusCode = httpStatusCode;
    }
    
    public int getHttpStatusCode()
    {
        return mHttpStatusCode;
    }
}

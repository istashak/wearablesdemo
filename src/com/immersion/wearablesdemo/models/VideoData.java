package com.immersion.wearablesdemo.models;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import com.immersion.wearablesdemo.utils.FileUtils;
import com.immersion.wearablesdemo.utils.VideoDataHelper;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Must implement {@link Parcelable} and not {@link Serializable} in order to pass {@link Bitmap}s.
 * 
 * @author ivan stashak
 *
 */
public class VideoData implements Parcelable
{	
	private int pageIndex;
	private int videoIndex;
	
	private String pageTitle;
	private String pageDescription;
	
	private String videoTitle;
	
	private String videoUri;
	private String hapticUri;
	
	private String iconAssetUri;
	private String whiteIconAssetUri;
	private String videoBitmapAssetUri;
	
	
	public VideoData(final Builder builder)
	{
		this.pageIndex = builder.pageIndex;
		this.videoIndex = builder.videoIndex;
		
		this.pageTitle = builder.pageTitle;
		this.pageDescription = builder.pageDescription;
		
		this.videoTitle = builder.videoTitle;
		
		this.videoUri = builder.videoUri;
		this.hapticUri = builder.hapticUri;
		
		this.iconAssetUri = builder.iconAssetUri;
		this.whiteIconAssetUri = builder.whiteIconAssetUri;
		this.videoBitmapAssetUri = builder.videoBitmapAssetUri;
	}
	
	
	public VideoData(Parcel source)
    {
        pageIndex = source.readInt();
        videoIndex = source.readInt();
        
        pageTitle = source.readString();
        pageDescription = source.readString();
        
        videoTitle = source.readString();
        
        videoUri = source.readString();
        hapticUri = source.readString();
        
        iconAssetUri = source.readString();
        whiteIconAssetUri = source.readString();
        videoBitmapAssetUri = source.readString();
    }
	
	public int getPageIndex()
	{
		return pageIndex;
	}
	
	public int getVideoIndex()
	{
		return videoIndex;
	}
	
	
	
	public String getPageTitle()
	{
        return pageTitle;
	}
	
	public String getPageDescription()
	{
        return pageDescription;
	}
	
	public String getVideoTitle()
	{
		return videoTitle;
	}
	
	
	public String getVideoUri(){
		return videoUri;
	}
	
	public String getHapticUri(){
		return hapticUri;
	}
	
	public String getIconAssetUri()
	{
		return iconAssetUri;
	}
	
	public String getWhiteIconAssetUri()
	{
		return whiteIconAssetUri;
	}
	
	public String getVideoBitmapAssetUri()
	{
		return videoBitmapAssetUri;
	}
	
	public Bitmap getIconForPage(){
		
        return VideoDataHelper.getBitmap(iconAssetUri);
	}
	
	public Bitmap getWhiteIconForPage(){
		
        return VideoDataHelper.getBitmap(whiteIconAssetUri);
	}
	
	public Bitmap getMovieImage()
	{
        return VideoDataHelper.getBitmap(videoBitmapAssetUri);
	}
	
	
	
	@Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
    	dest.writeInt(pageIndex);
    	dest.writeInt(videoIndex);
    	
        dest.writeString(pageTitle);
        dest.writeString(pageDescription);
        dest.writeString(videoTitle);
        dest.writeString(videoUri);
        dest.writeString(hapticUri);
        
        dest.writeString(iconAssetUri);
        dest.writeString(whiteIconAssetUri);
        dest.writeString(videoBitmapAssetUri);
    }

    public static final Parcelable.Creator<VideoData> CREATOR = new Parcelable.Creator<VideoData>()
    {
        @Override
        public VideoData createFromParcel(Parcel source)
        {
            return new VideoData(source);
        }

        @Override
        public VideoData[] newArray(int size)
        {
            return new VideoData[size];
        }
    };
    
    public static class Builder
    {
		private int pageIndex;
    	private int videoIndex;
    	
    	private String pageTitle;
    	private String pageDescription;
    	
    	private String videoTitle;
    	
    	private String videoUri;
    	private String hapticUri;
    	
    	private String iconAssetUri;
    	private String whiteIconAssetUri;
    	private String videoBitmapAssetUri;
		
    	
    	public Builder setPageIndex(final int pageIndex)
    	{
    		this.pageIndex = pageIndex;
    		return this;
    	}
    	
    	public Builder setVideoIndex(final int videoIndex)
    	{
    		this.videoIndex = videoIndex;
    		return this;
    	}
    	
    	public Builder setPageTitle(final String pageTitle)
    	{
    		this.pageTitle = pageTitle;
    		return this;
    	}
    	
    	public Builder setPageDescription(final String pageDescription)
    	{
    		this.pageDescription = pageDescription;
    		return this;
    	}
    	
    	public Builder setVideoTitle(final String videoTitle)
    	{
    		this.videoTitle = videoTitle;
    		return this;
    	}
    	
    	public Builder setVideoUri(final String videoUri)
    	{
    		this.videoUri = videoUri;
    		return this;
    	}
    	
    	public Builder setHapticUri(final String hapticUri)
    	{
    		this.hapticUri = hapticUri;
    		return this;
    	}
    	
    	/*
    	public Builder setIconBitmap(final Bitmap iconBitmap)
    	{
    		this.iconBitmap = iconBitmap;
    		return this;
    	}
    	
    	public Builder setVideoBitmap(final Bitmap videoBitmap)
    	{
    		this.videoBitmap = videoBitmap;
    		return this;
    	}
    	*/
    	public Builder setIconAssetUri(final String iconAssetUri)
    	{
    		this.iconAssetUri = iconAssetUri;
    		return this;
    	}

    	public Builder setWhiteIconAssetUri(final String whiteIconAssetUri)
    	{
    		this.whiteIconAssetUri = whiteIconAssetUri;
    		return this;
    	}
    	
    	public Builder setVideoBitmapAssetUri(final String videoBitmapAssetUri)
    	{
    		this.videoBitmapAssetUri = videoBitmapAssetUri;
    		return this;
    	}
    	
    	
    	public VideoData build()
    	{
    		return new VideoData(this);
    	}
    }
}

package com.immersion.wearablesdemo.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * An immutable class to hold and pass haptic file information across thread
 * boundaries.
 * 
 * @author ivan stashak
 * 
 */
public class HapticFileInformation implements Parcelable
{
    private String mFilePath;
    private int mTotalFileLength;

    private int mMajorVersion;
    private int mMinorVersion;

    private int mEncoding;

    private int mSampleHertz;
    private int mBitsPerSample;

    private int mNumberOfChannels;
    private int mActuatorArray[];
    private int mCompressionScheme;

    private int mHapticDataLength;
    private int mHapticDataStartByteOffset;

    public HapticFileInformation()
    {
    }

    private HapticFileInformation(final Builder builder)
    {
        mFilePath = builder.mFilePath;
        mTotalFileLength = builder.mTotalFileLength;

        mMajorVersion = builder.mMajorVersion;
        mMinorVersion = builder.mMinorVersion;

        mEncoding = builder.mEncoding;

        mSampleHertz = builder.mSampleHertz;
        mBitsPerSample = builder.mBitsPerSample;

        mNumberOfChannels = builder.mNumberOfChannels;
        mActuatorArray = builder.mActuatorArray;
        mCompressionScheme = builder.mCompressionScheme;

        mHapticDataLength = builder.mHapticDataLength;
        mHapticDataStartByteOffset = builder.mHapticDataStartByteOffset;
    }

    public HapticFileInformation(Parcel source)
    {
        mFilePath = source.readString();
        mTotalFileLength = source.readInt();

        mMajorVersion = source.readInt();
        mMinorVersion = source.readInt();

        mSampleHertz = source.readInt();
        mBitsPerSample = source.readInt();

        mNumberOfChannels = source.readInt();
        mActuatorArray = new int[mNumberOfChannels];

        for (int i = 0; i < mNumberOfChannels; ++i)
        {
            mActuatorArray[i] = source.readInt();
        }

        mCompressionScheme = source.readInt();

        mHapticDataLength = source.readInt();
        mHapticDataStartByteOffset = source.readInt();
    }

    public String getFilePath()
    {
        return mFilePath;
    }

    public int getFileLength()
    {
        return mTotalFileLength;
    }

    public int getMajorVersion()
    {
        return mMajorVersion;
    }

    public int getMinorVersion()
    {
        return mMinorVersion;
    }

    public int getEncoding()
    {
        return mEncoding;
    }

    public int getSampleHertz()
    {
        return mSampleHertz;
    }

    public int getBitsPerSample()
    {
        return mBitsPerSample;
    }

    public int getNumberOfChannels()
    {
        return mNumberOfChannels;
    }

    public int[] getActuatorArray()
    {
        return mActuatorArray;
    }

    public int getCompressionScheme()
    {
        return mCompressionScheme;
    }

    public int getHapticDataLength()
    {
        return mHapticDataLength;
    }

    public int getHapticDataStartByteOffset()
    {
        return mHapticDataStartByteOffset;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(mFilePath);
        dest.writeInt(mTotalFileLength);

        dest.writeInt(mMajorVersion);
        dest.writeInt(mMinorVersion);

        dest.writeInt(mEncoding);

        dest.writeInt(mSampleHertz);
        dest.writeInt(mBitsPerSample);
        dest.writeInt(mNumberOfChannels);

        for (int i = 0; i < mNumberOfChannels; ++i)
        {
            dest.writeInt(mActuatorArray[i]);
        }

        dest.writeInt(mCompressionScheme);

        dest.writeInt(mHapticDataLength);
        dest.writeInt(mHapticDataStartByteOffset);
    }

    public static final Parcelable.Creator<HapticFileInformation> CREATOR = new Parcelable.Creator<HapticFileInformation>()
    {
        @Override
        public HapticFileInformation createFromParcel(Parcel source)
        {
            return new HapticFileInformation(source);
        }

        @Override
        public HapticFileInformation[] newArray(int size)
        {
            return new HapticFileInformation[size];
        }
    };

    public static class Builder
    {
        private String mFilePath;
        private int mTotalFileLength;

        private int mMajorVersion;
        private int mMinorVersion;

        private int mEncoding;

        private int mSampleHertz;
        private int mBitsPerSample;

        private int mNumberOfChannels;
        private int mActuatorArray[];
        private int mCompressionScheme;

        private int mHapticDataLength;
        private int mHapticDataStartByteOffset;

        public Builder()
        {
        }

        public Builder setFilePath(final String filePath)
        {
            mFilePath = filePath;
            return this;
        }

        public Builder setTotalFileLength(final int totalFileLength)
        {
            mTotalFileLength = totalFileLength;
            return this;
        }

        public Builder setMajorVersion(final int majorVersion)
        {
            mMajorVersion = majorVersion;
            return this;
        }

        public Builder setMinorVersion(final int minorVersion)
        {
            mMinorVersion = minorVersion;
            return this;
        }

        public Builder setEncoding(final int encoding)
        {
            mEncoding = encoding;
            return this;
        }

        public Builder setSampleHertz(final int sampleHertz)
        {
            mSampleHertz = sampleHertz;
            return this;
        }

        public Builder setBitsPerSample(final int bitsPerSample)
        {
            mBitsPerSample = bitsPerSample;
            return this;
        }

        public Builder setNumberOfChannels(final int numberOfChannels)
        {
            mNumberOfChannels = numberOfChannels;
            return this;
        }

        public Builder setActuatorArray(final int[] actuatorArray)
        {
            mActuatorArray = actuatorArray;
            return this;
        }

        public Builder setCompressionScheme(final int compressionScheme)
        {
            mCompressionScheme = compressionScheme;
            return this;
        }

        public Builder setHapticDataLength(final int hapticDataLength)
        {
            mHapticDataLength = hapticDataLength;
            return this;
        }

        public Builder setHapticDataStartByteOffset(
                final int hapticDataStartByteOffset)
        {
            mHapticDataStartByteOffset = hapticDataStartByteOffset;
            return this;
        }

        public HapticFileInformation build()
        {
            return new HapticFileInformation(this);
        }
    }

}

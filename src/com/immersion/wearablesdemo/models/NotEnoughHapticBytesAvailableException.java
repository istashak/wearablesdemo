package com.immersion.wearablesdemo.models;

public class NotEnoughHapticBytesAvailableException extends Exception
{
    public NotEnoughHapticBytesAvailableException(final String message)
    {
        super(message);
    }
}

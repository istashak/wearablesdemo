package com.immersion.wearablesdemo.models;

public class BeyondHapticDataException extends Exception
{
    public BeyondHapticDataException(final String message)
    {
        super(message);
    }
}

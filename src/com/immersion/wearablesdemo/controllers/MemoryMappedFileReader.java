package com.immersion.wearablesdemo.controllers;

import android.util.Log;

import com.immersion.wearablesdemo.models.HapticFileInformation;
import com.immersion.wearablesdemo.models.NotEnoughHapticBytesAvailableException;
import com.immersion.wearablesdemo.utils.FileUtils;
import com.immersion.wearablesdemo.utils.Profiler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;

public class MemoryMappedFileReader implements IHapticFileReader
{
    private static final String TAG = "MemoryMappedFileReader";

    private File mFile;
    private FileChannel mFileChannel;

    private MemoryMapWrapper mCurrentMMW;
    private MemoryMapWrapper mNextMMW;

    private int mBytesAvailable;

    private static int mBlockSizeMS = 40;
    private static int mBlockSizeBytes = 0;

    private HapticFileInformation mHapticFileInfo;

    private static final int PAGE_SIZE = 4096;
    private static final int THREE_PAGE_SIZES = PAGE_SIZE * 3;


    private final Profiler mProfiler = new Profiler();


    @Override
    public void setBlockSizeMS(int blockSizeMS)
    {
        mBlockSizeMS = blockSizeMS;
    }

    @Override
    public void setBytesAvailable(int bytesAvailable)
    {
        Log.d(TAG, "setBytesAvailable(" + bytesAvailable + ")");

        mBytesAvailable = bytesAvailable;

        initializeFileRead();
    }

    private boolean bytesAreAvailableInFile(final int numBytes)
    {
        return mBytesAvailable >= numBytes;
    }

    /**
     * This function creates a {@link FileChannel} and a {@link HapticFileInformation} object.  If successful or already done the
     * funciton returns true.
     *
     * @return True if completed successfully or already done, false otherwise.
     */
    private boolean initializeFileRead()
    {
        //Log.d(TAG, "initializeFileRead()");

        RandomAccessFile randomAccessFile = null;

        try
        {
            if(mHapticFileInfo != null)
            {
                return true;
            }

            if(bytesAreAvailableInFile(THREE_PAGE_SIZES))
            {
                return false;
            }

            if(mFile == null)
            {
                mFile = FileUtils.getHapticStorageFile();
            }

            if (mFileChannel == null)
            {
                randomAccessFile = new RandomAccessFile(mFile, "r");
                mFileChannel = randomAccessFile.getChannel();
            }

            if (mFileChannel == null)
            {
                Log.e(TAG, "did not create file channle!");
                return false;
            }

            Log.d(TAG, "FileChannel.size() = " + mFileChannel.size());

            return readHapticFileHeader();
        }
        catch (FileNotFoundException e)
        {
            Log.e(TAG, "FileNotFoundException", e);

            FileUtils.closeCloseable(randomAccessFile);
            FileUtils.closeCloseable(mFileChannel);
        } catch (IOException e)
        {
            Log.d(TAG, "IOException", e);
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return false;
    }

    private boolean readHapticFileHeader()
    {
        Log.d(TAG, "************* readHapticFileHeader() beginning *************");

        try
        {
            final ByteBuffer tmp = ByteBuffer.allocate(4);

            tmp.order(ByteOrder.LITTLE_ENDIAN);
            tmp.position(0);

            // Find the size of 'HAPT''fmt ' header info.
            int bytesRead = mFileChannel.read(tmp, 0x10);

            if (bytesRead != 4)
            {
                Log.d(TAG, "************* readHapticFileHeader() NOT successful *************");
                return false;
            }

            tmp.flip();
            final int size = tmp.getInt() + 28;

            // Now allocate a buffer big enough to hold all of the header data.
            final ByteBuffer allHeaderInfo = ByteBuffer.allocate(size);
            allHeaderInfo.order(ByteOrder.LITTLE_ENDIAN);

            // Now read the entire header into a buffer, so that we can parse
            // through it.
            bytesRead = mFileChannel.read(allHeaderInfo, 0x00);

            if (bytesRead != size)
            {
                Log.d(TAG, "************* readHapticFileHeader() NOT successful *************");
                return false;
            }

            allHeaderInfo.flip();

            // Start parsing the file header
            final HapticFileInformation.Builder b = new HapticFileInformation.Builder();

            b.setFilePath(mFile.getAbsolutePath());

            allHeaderInfo.position(4);
            b.setTotalFileLength(allHeaderInfo.getInt() + 8);

            // jump a head to the pertinent haptic info
            allHeaderInfo.position(0x14);
            b.setMajorVersion(allHeaderInfo.get());

            b.setMinorVersion(allHeaderInfo.get());

            b.setEncoding(allHeaderInfo.get());

            // skip the reserved byte and jump to the sample rate
            allHeaderInfo.position(0x18);
            b.setSampleHertz(allHeaderInfo.getInt());

            final int bls = allHeaderInfo.get();
            final int bhs = allHeaderInfo.get();

            b.setBitsPerSample((bhs << 8) | bls);

            final int numChannels = allHeaderInfo.get();

            b.setNumberOfChannels(numChannels);

            final int[] actuatorArray = new int[numChannels];

            for (int i = 0; i < numChannels; ++i)
            {
                actuatorArray[i] = allHeaderInfo.get();
            }

            b.setActuatorArray(actuatorArray);

            b.setCompressionScheme(allHeaderInfo.get());

            // skip across the 'data' box
            allHeaderInfo.position(allHeaderInfo.position() + 4);
            b.setHapticDataLength(allHeaderInfo.getInt());

            b.setHapticDataStartByteOffset(allHeaderInfo.position());

            mHapticFileInfo = b.build();

            int samplesPerBuffer = mBlockSizeMS * mHapticFileInfo.getSampleHertz() / 1000;
            mBlockSizeBytes = samplesPerBuffer * mHapticFileInfo.getBitsPerSample() * mHapticFileInfo.getNumberOfChannels() / 8;

            Log.d(TAG, "************* readHapticFileHeader() successful *************");
        }
        catch (IOException e)
        {
            Log.e(TAG, "FileNotFoundException", e);

            return false;
        }

        return true;
    }

    @Override
    public boolean bufferAtPlaybackPosition(final int playbackPositionMS)
    {
        Log.d(TAG, "-------------- startBufferingAtPlaybackPosition() beginning --------------");

        if (!initializeFileRead())
        {
            return false;
        }

        final int bytePositionInHapticData = calculateByteOffsetIntoHapticData(playbackPositionMS);

        Log.d(TAG, "playbackPositionMS = " + playbackPositionMS);
        Log.d(TAG, "calculated byte position  = " + bytePositionInHapticData);

        if(mCurrentMMW != null)
        {
            Log.d(TAG, "mCurrentMMW position = " + mCurrentMMW.mMappedByteBuffer.position());
            Log.d(TAG, "mCurrentMMW mHapticDataOffset + position = " + (mCurrentMMW.mHapticDataOffset + mCurrentMMW.mMappedByteBuffer.position()));
        }

        Log.d(TAG, "-------------------------------------------------------------------------");

        // check to see if the current buffer is out of range
        if (mCurrentMMW == null || bytePositionIsOutOfRange(mCurrentMMW, bytePositionInHapticData))
        {
            try
            {
                // check to see if the next buffer is out of range or about to
                // be out of range. If the conditions are true the
                // playback has seeked beyond or below any of the data that is
                // held in the buffers so flush and rebuffer.
                if (mNextMMW == null || bytePositionIsOutOfRange(mNextMMW, bytePositionInHapticData) || bytePositionCausesHighSideOverlap(mNextMMW, bytePositionInHapticData))
                {
                    if (mCurrentMMW == null || mCurrentMMW.mHapticDataOffset != bytePositionInHapticData)
                    {
                        mCurrentMMW = getMemoryMapWrapper(bytePositionInHapticData);
                    }

                    if (mNextMMW == null || mNextMMW.mHapticDataOffset != bytePositionInHapticData + PAGE_SIZE)
                    {
                        mNextMMW = getMemoryMapWrapper(bytePositionInHapticData + PAGE_SIZE);
                    }

                    //logBufferState();

                    if(mCurrentMMW != null)
                    {
                        Log.d(TAG, "mCurrentMMW position = " + mCurrentMMW.mMappedByteBuffer.position());
                        Log.d(TAG, "mCurrentMMW mHapticDataOffset + position = " + (mCurrentMMW.mHapticDataOffset + mCurrentMMW.mMappedByteBuffer.position()));
                    }

                    Log.d(TAG, "^^^^^^^^^^^^^^^^^^^ startBufferingAtPlaybackPosition() end ^^^^^^^^^^^^^^^^^^^^^^^");

                    // Here we can just return because the position of the current buffer will appropriately be set to zero
                    return true;
                }
                // This case is when the position has moved into the next
                // buffer, so we can simply have the current buffer point at it
                // ... Meanwhile page
                // we also pull the next page into memory.
                else
                {
                    swapBuffersAndPage();
                }
            }
            catch (NotEnoughHapticBytesAvailableException e)
            {
                Log.e(TAG, e.getMessage(), e);
                return false;
            }
            catch (IOException e)
            {
                Log.e(TAG, e.getMessage(), e);
                return false;
            }
        }

        // Here we ensure that mCurrentMMW is not null for cases when the user seeked to the end of the data.
        if(mCurrentMMW != null)
        {
            mCurrentMMW.mMappedByteBuffer.position(calculateByteOffsetIntoPage(mCurrentMMW, bytePositionInHapticData));
        }

        //logBufferState();

        Log.d(TAG, "mCurrentMMW position = " + mCurrentMMW.mMappedByteBuffer.position());
        Log.d(TAG, "mCurrentMMW mHapticDataOffset + position = " + (mCurrentMMW.mHapticDataOffset + mCurrentMMW.mMappedByteBuffer.position()));

        Log.d(TAG, "^^^^^^^^^^^^^^^^^^^ startBufferingAtPlaybackPosition() end ^^^^^^^^^^^^^^^^^^^^^^^");

        return true;
    }



    private void swapBuffersAndPage() throws NotEnoughHapticBytesAvailableException, IOException //, BeyondHapticDataException
    {
        Log.d(TAG, "------------ swapBuffersAndPage() Beginning ------------");

        if(mNextMMW == null)
        {
            return;
        }

        // Calculate the next offset for the next page
        final int nextPageOffset = mNextMMW.mHapticDataOffset + PAGE_SIZE;

        Log.d(TAG, "nextPageOffset = " + nextPageOffset);

        // swap buffers
        mCurrentMMW = mNextMMW;

        // Put the next page of the file into memory.
        mNextMMW = getMemoryMapWrapper(nextPageOffset);

        Log.d(TAG, "-------- swapBuffersAndPage() Successful Ending ---------");
    }


    @Override
    public byte[] getBufferForPlaybackPosition(final int playbackPositionMS) throws NotEnoughHapticBytesAvailableException
    //public byte[] getBufferForPlaybackPosition() throws NotEnoughHapticBytesAvailableException
    {
        // here we are only assuming one channel of usage

        if(mCurrentMMW == null)
        {
            return null;
        }

        // This variable represents the position that the buffer was positioned to by the immediately preceding call to
        // bufferAtPlaybackPosition(int playbackPositionMS).
        final int bytePositionInHapticData = mCurrentMMW.mHapticDataOffset + mCurrentMMW.mMappedByteBuffer.position();

        if (bytePositionInHapticData >= mHapticFileInfo.getHapticDataLength())
        {
            Log.d(TAG, "End of haptic data reached");
            return null;
        }

        /*
        final int bytePositionInHapticData = mCurrentMMW.mHapticDataOffset + mCurrentMMW.mMappedByteBuffer.position();

        // This variable represents the position that the haptic data should be at based off the passed in value of
        // the parameter playbackPositionMS.  This, in theory should never differ.
        final int calculatedBytePositionInHapticData = calculateByteOffsetIntoHapticData(playbackPositionMS);

        if (calculatedBytePositionInHapticData >= mHapticFileInfo.getHapticDataLength())
        {
            Log.d(TAG, "End of haptic data reached");
            return null;
        }
        // This code adjusts the position of the
        else if(bytePositionInHapticData != calculatedBytePositionInHapticData)
        {
            if(bytePositionIsOutOfRange(mCurrentMMW, calculatedBytePositionInHapticData))
            {
                mCurrentMMW.mMappedByteBuffer.position(calculateByteOffsetIntoPage(mCurrentMMW, calculatedBytePositionInHapticData));
            }

            Log.d(TAG, "!!!!!!!!!!!!!! OUT OF SYNC !!!!!!!!!!!!!!!!!!!");

            Log.d(TAG, "playbackPositionMS = " + playbackPositionMS);
            Log.d(TAG, "calculated byte position = " + calculatedBytePositionInHapticData);
            Log.d(TAG, "mCurrentMMW position = " + mCurrentMMW.mMappedByteBuffer.position());
            Log.d(TAG, "mCurrentMMW mHapticDataOffset + position = " + bytePositionInHapticData);

            Log.d(TAG, "bytePositionInHapticData = " + bytePositionInHapticData);
            Log.d(TAG, "haptic data length = " + mHapticFileInfo.getHapticDataLength());

            //logBufferState();
            Log.d(TAG, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }
        */

        // Test code to be removed!!!
        /*
        if(bytePositionInHapticData != calculatedPosition)
        {
            Log.d(TAG, "!!!!!!!!!!!!!! OUT OF SYNC !!!!!!!!!!!!!!!!!!!");

            Log.d(TAG, "playbackPositionMS = " + playbackPositionMS);
            Log.d(TAG, "calculated byte position = " + calculatedPosition);
            Log.d(TAG, "mCurrentMMW position = " + mCurrentMMW.mMappedByteBuffer.position());
            Log.d(TAG, "mCurrentMMW mHapticDataOffset + position = " + bytePositionInHapticData);

            Log.d(TAG, "bytePositionInHapticData = " + bytePositionInHapticData);
            Log.d(TAG, "haptic data length = " + mHapticFileInfo.getHapticDataLength());

            //logBufferState();
            Log.d(TAG, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

            throw new RuntimeException("!!!!! OUT OF SYNC ... bytePositionInHapticData != calculatedPosition !!!!!");
        }
        */

        byte[] buf = null;

        // Log.d(TAG, "byte position into page[" + bytePosition + "]");
        // Log.d(TAG, "byte position into haptic data[" + bytePositionInHapticData + "]");

        try
        {
            buf = new byte[mBlockSizeBytes];

            if (mBlockSizeBytes >= mCurrentMMW.mMappedByteBuffer.remaining())
            {
                final int n = mCurrentMMW.mMappedByteBuffer.remaining();
                int n2 = mBlockSizeBytes - n;

                mCurrentMMW.mMappedByteBuffer.get(buf, 0, n);

                // Check to see if we need to read from the next buffer and if the next buffer exists -- this is a boundary
                // condition. If true, this case represents the scenario when the return buffer strattles the current and next buffer.
                if(n2 > 0 && mNextMMW != null)
                {
                    n2 = mNextMMW.mMappedByteBuffer.remaining() >= n2 ? n2 : mNextMMW.mMappedByteBuffer.remaining();

                    mNextMMW.mMappedByteBuffer.get(buf, n, n2);
                }

                swapBuffersAndPage();
            }
            else
            {
                mCurrentMMW.mMappedByteBuffer.get(buf, 0, mBlockSizeBytes);
            }
        }
        catch (BufferUnderflowException e)
        {
            Log.e(TAG, e.getMessage(), e);

            logBufferState();

            throw e;
        }
        catch (IndexOutOfBoundsException e)
        {
            Log.e(TAG, e.getMessage(), e);

            logBufferState();

            throw e;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return buf;
    }


    /**
     * This function uses the AV playback position to return the byte offset of the corresponding haptic data from the
     * beginning of the haptic data in the haptic file.  Note this is different then the total offset from the beginning of
     * the file that the function {@code calculateByteOffsetInFileToSample(final int playbackPositionMS)} gives.
     *
     * @param playbackPositionMS -- The AV playback position in milliseconds.
     * @return The byte offset, into the haptic data, of the haptic data corresponding to the AV timecode.
     */
    private int calculateByteOffsetIntoHapticData(final int playbackPositionMS)
    {
        // By using an int type for milliseconds PerSample we assume we won't
        // exceed 1,000 Hz
        final int millisecondsPerSample = 1000 / mHapticFileInfo.getSampleHertz();
        final int sampleNumber = playbackPositionMS / millisecondsPerSample;
        final int bitsPerSample = mHapticFileInfo.getBitsPerSample();
        final int numChannels = mHapticFileInfo.getNumberOfChannels();

        final float sampleByteSizeFloat = (bitsPerSample * numChannels) / 8.0f;
        final float sampleByteSizeFloatFloor = (bitsPerSample * numChannels) / 8;

        int sampleByteSize = (int) sampleByteSizeFloatFloor;

        // If the sampleByteSizeFloat is larger than it's floor value then we
        // know the bits per sample doesn't fall on a byte
        // boundary (i.e. isn't an exact multiple of 8).
        if (sampleByteSizeFloat > sampleByteSizeFloatFloor)
        {
            ++sampleByteSize;
        }

        return sampleByteSize * sampleNumber;
    }



    /**
     * This function uses the AV playback position to find the offset into the current page/buffer of the corresponding haptic data.
     *
     * @param playbackPositionMS -- The AV playback position in milliseconds.
     * @return The relative byte offset, into the current page/buffer, of the haptic data corresponding to the AV timecode.
     */
    private int calculateByteOffsetIntoPage(final MemoryMapWrapper mmw, final int byteOffsetIntoHapticData)
    {
        return (byteOffsetIntoHapticData - mmw.mHapticDataOffset) % mmw.mMappedByteBuffer.capacity();
    }


    /**
     * Uses the AV playback position to return the byte offset of the corresponding haptic data from the
     * beginning of the haptic file.
     *
     * @param playbackPositionMS -- The AV playback position in milliseconds.
     * @return The byte offset, into the hapt file, of the haptic data corresponding to the AV timecode.
     */
    private int calculateByteOffsetInFileToSample(final int playbackPositionMS)
    {
        return mHapticFileInfo.getHapticDataStartByteOffset() + calculateByteOffsetIntoHapticData(playbackPositionMS);
    }


    @Override
    public void close()
    {
        FileUtils.closeCloseable(mFileChannel);
    }

    /**
     * A light weight class for wrapping a MappedByteBuffer
     *
     * @author ivan stashak
     *
     */
    private static class MemoryMapWrapper
    {
        /**
         * The offset of this buffer into the haptic data.
         */
        public int mHapticDataOffset;

        public MappedByteBuffer mMappedByteBuffer;
    }


    /**
     * Retrieves a {@link MemoryMapWrapper} for a given byte offset into the haptic data.
     *
     * @param hapticDataByteOffset -- A byte offset into the haptic data.
     * @return A {@link MemoryMapWrapper} if the memory map was successful, or null if no more data exists.
     *
     * @throws IOException
     * @throws NotEnoughHapticBytesAvailableException -- This informs when buffering requests exceed what the HapticDownloadThread has been able to write to file.
     */
    private MemoryMapWrapper getMemoryMapWrapper(final int hapticDataByteOffset) throws IOException, NotEnoughHapticBytesAvailableException
    {
        Log.d(TAG, "getMemoryMapWrapper() haptic data offset = " + hapticDataByteOffset);

        mProfiler.startTiming();

        MemoryMapWrapper mmw = null;

        if(hapticDataByteOffset < mHapticFileInfo.getHapticDataLength())
        {
            final int fileOffset = mHapticFileInfo.getHapticDataStartByteOffset() + hapticDataByteOffset;

            final int mapSize = (hapticDataByteOffset + PAGE_SIZE) <= mHapticFileInfo.getHapticDataLength() ? PAGE_SIZE : mHapticFileInfo.getHapticDataLength() - hapticDataByteOffset;

            if(hapticDataByteOffset + mapSize > mBytesAvailable)
            {
                throw new NotEnoughHapticBytesAvailableException("Not enough bytes available yet.");
            }

            final MappedByteBuffer mbb = mFileChannel.map(MapMode.READ_ONLY, fileOffset, mapSize);

            if(mbb != null)
            {
                mbb.order(ByteOrder.LITTLE_ENDIAN);

                mmw = new MemoryMapWrapper();

                mmw.mMappedByteBuffer = mbb;
                mmw.mHapticDataOffset = hapticDataByteOffset;
            }

            Log.d(TAG, "getMemoryMapWrapper() duration ---> " + mProfiler.getDuration());
        }

        return mmw;
    }


    //
    private static boolean bytePositionIsBelowRange(final MemoryMapWrapper memoryMapWrapper, final int byteOffsetIntoHapticData)
    {
        return byteOffsetIntoHapticData < memoryMapWrapper.mHapticDataOffset;
    }

    private static boolean bytePositionIsAboveRange(final MemoryMapWrapper memoryMapWrapper, final int byteOffsetIntoHapticData)
    {
        return byteOffsetIntoHapticData >= memoryMapWrapper.mHapticDataOffset + memoryMapWrapper.mMappedByteBuffer.capacity();
    }

    private static boolean bytePositionIsOutOfRange(final MemoryMapWrapper memoryMapWrapper, final int byteOffsetIntoHapticData)
    {
        return bytePositionIsBelowRange(memoryMapWrapper, byteOffsetIntoHapticData) || bytePositionIsAboveRange(memoryMapWrapper, byteOffsetIntoHapticData);
    }

    private static boolean bytePositionCausesHighSideOverlap(final MemoryMapWrapper memoryMapWrapper, final int byteOffsetIntoHapticData)
    {
        return bytePositionIsAboveRange(memoryMapWrapper, byteOffsetIntoHapticData + mBlockSizeBytes);
    }

    @Override
    public HapticFileInformation getHapticFileInformation()
    {
        return mHapticFileInfo;
    }

    private void logBufferState()
    {
        Log.d(TAG, "%%%%%%%%%%% logBufferState %%%%%%%%%%%");


        if(mCurrentMMW != null)
        {
            Log.d(TAG, "mCurrentMMW capacity = " + mCurrentMMW.mMappedByteBuffer.capacity());
            Log.d(TAG, "mCurrentMMW position = " + mCurrentMMW.mMappedByteBuffer.position());
            Log.d(TAG, "mCurrentMMW remaining = " + mCurrentMMW.mMappedByteBuffer.remaining());
            Log.d(TAG, "mCurrentMMW mHapticDataOffset = " + mCurrentMMW.mHapticDataOffset);
            Log.d(TAG, "mCurrentMMW mHapticDataOffset + position = " + (mCurrentMMW.mHapticDataOffset + mCurrentMMW.mMappedByteBuffer.position()));
        }
        else
        {
            Log.d(TAG, "mCurrentMMW is null");
        }

        Log.d(TAG, "--------------------------------------");

        if(mNextMMW != null)
        {
            Log.d(TAG, "mNextMMW capacity = " + mNextMMW.mMappedByteBuffer.capacity());
            Log.d(TAG, "mNextMMW position = " + mNextMMW.mMappedByteBuffer.position());
            Log.d(TAG, "mNextMMW remaining = " + mNextMMW.mMappedByteBuffer.remaining());
            Log.d(TAG, "mNextMMW mHapticDataOffset = " + mNextMMW.mHapticDataOffset);
            Log.d(TAG, "mNextMMW mHapticDataOffset + position = " + (mNextMMW.mHapticDataOffset + mNextMMW.mMappedByteBuffer.position()));
        }
        else
        {
            Log.d(TAG, "mNextMMW is null");
        }

        Log.d(TAG, "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    }
}

package com.immersion.wearablesdemo.controllers;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.immersion.wearablesdemo.utils.FileUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;


public class BluetoothCommunication implements Closeable
{
    /** String to identify app */
    private final static UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
    
    public static final int REQUEST_ENABLE_BT = 666;

    private BluetoothAdapter mBluetoothAdapter;

    private Context mContext;

    private ConnectionThread mConnectionThread;

    private ConnectThread mConnectThread;

    /** Thread responsible for starting Bt server */
    private AcceptThread mAcceptThread;

    private boolean mIsBluetoothSupported = false;
    

    
    private BluetoothSocket mBluetoothSocket = null;
    
    private volatile boolean isConnecting;
    private volatile boolean isConnected;
    
    private volatile boolean isClosed;

    private BluetoothUICallbackHandler mHandler;
    

    private static final String TAG = BluetoothCommunication.class.getSimpleName();
    
    
    

    private static BluetoothCommunication bluetoothSingleton;
    
    
    public static void createBluetoothSingleton(final Context context)
    {
    	if(bluetoothSingleton == null || bluetoothSingleton.isClosed)
    	{
    		bluetoothSingleton = new BluetoothCommunication(context.getApplicationContext());
    	}
    }
    
    public static BluetoothCommunication getInstance()
    {
    	return bluetoothSingleton;
    }
    
    @Override
    public void close() throws IOException
	{
		isClosed = true;
		stopBluetoothCommunication();
		
		bluetoothSingleton = null;
	}
    
    private BluetoothCommunication (final Context context)
    {
    	isClosed = false;
    	
    	mContext = context;
    	mHandler = new BluetoothUICallbackHandler(context);
    	
    	// Ivan: Documentation says to use getSystemService(Context.BLUETOOTH_SERVICE) for sdk 17 and higher,
    	// instead of BluetoothAdapter.getDefaultAdapter() ... leaving it as is for now.
    	// mBluetoothAdapter = (BluetoothAdapter)mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // Ivan: I just put this code here for demonstrative purposes.
        if(mBluetoothAdapter == null)
        {
        	mIsBluetoothSupported = false;
        }   
    }

    public boolean isBluetoothEnabled()
    {
    	return mBluetoothAdapter.isEnabled();
    }

    public synchronized void startConnectThread()
    {	
    	stopBluetoothCommunication();
    	
    	final BluetoothDevice device = findPairedWatch();

    	if(device != null)
    	{
    		isConnecting = true;
    		
    		mConnectThread = new ConnectThread(device);
    		mConnectThread.start();
    		
    		mHandler.postOnConnecting();
    	}
    }
    
    
    public synchronized void startAcceptThread()
    {
    	stopBluetoothCommunication();
    	
    	isConnecting = true;
    	
    	mAcceptThread = new AcceptThread();
    	mAcceptThread.start();
    	
    	mHandler.postOnConnecting();
    }
    
    
    public synchronized boolean isConnectedOrConnecting()
    {
    	return isConnected || isConnecting;
    }
    
    public synchronized boolean isConnecting()
    {
    	return isConnecting;
    }
    
    
    public synchronized boolean isConnected()
    {
    	// Ivan: for some reason mBluetoothSocket.isConnected() returns true when the socket pipe is broken
    	//return mBluetoothSocket != null && mBluetoothSocket.isConnected();
    	return isConnected;
    }
    
    /**
     * If connected, return the currently connected {@link BluetoothDevice}, if not return null.
     * 
     * @return {@link BluetoothDevice} or null.
     */
    public synchronized BluetoothDevice getConnectedBluetoothDevice()
    {
    	if(isConnected)
    	{
    		return mBluetoothSocket.getRemoteDevice();
    	}
    	
    	return null;
    }
    
    public void stopBluetoothCommunication()
    {
    	stopAllConnectionAttempts();
    	stopConnectionThread();
    }
    
    
    private synchronized void closeBluetoothSocket()
    {
    	FileUtils.closeCloseable(mBluetoothSocket);
    	mBluetoothSocket = null;
    	isConnected = false;
    }
    
    private synchronized void stopConnectThread()
    {
    	if(mConnectThread != null)
    	{
    		mConnectThread.cancel();
    		mConnectThread = null;
    	}
    }
    
    private synchronized void stopAcceptThread()
    {
    	if(mAcceptThread != null)
		{
			mAcceptThread.cancel();
			mAcceptThread = null;
		}
    }
    
    private synchronized void stopAllConnectionAttempts()
    {
    	stopConnectThread();
    	stopAcceptThread();
    }
    
    /**
     * This function stops the ConnectionThread if it exists, which alerts any connected devices and then closes the 
     * underlying BluetoothSocket.
     */
    private synchronized void stopConnectionThread()
    {
    	if(mConnectionThread != null)
    	{
    		mConnectionThread.cancel();
    		mConnectionThread = null;
    	}
    }
   
    
    
    private BluetoothDevice findPairedWatch()
    {
        final Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        
        if (pairedDevices.size() > 0)
        {
            for (BluetoothDevice device : pairedDevices)
            {
            	final String name = device.getName();
            	
            	if(name.contains("Gear") && device.getBondState() == BluetoothDevice.BOND_BONDED)
            	{
            		return device;
            	}
            }
        }
        
        return null;
    }
    
    
    private synchronized void connected(final BluetoothSocket bluetoothSocket)
    {
    	Log.d(TAG, "connected called with " + (bluetoothSocket != null ? "an open socket." : "no open socket."));
    	
    	isConnecting = false;

    	if(bluetoothSocket != null)
    	{
    		isConnected = true;
    		
    		mBluetoothSocket = bluetoothSocket;

    		mConnectionThread = new ConnectionThread();
    		mConnectionThread.start();
    		
    		//queueToastMessage("Connected to " + mBluetoothSocket.getRemoteDevice().getName());
    		mHandler.postOnConnected(mBluetoothSocket.getRemoteDevice());
    	}
    	else
    	{
    		mHandler.postOnConnectionFailure();
    	}
    }

    
    private class AcceptThread extends Thread
    {
        private BluetoothServerSocket localServerSocket;

        public AcceptThread()
        {
        	super("Phone AcceptThread");
        	
        	Log.d(TAG, "AcceptThread Instantiated");
        	
            try
            {
                localServerSocket = mBluetoothAdapter.listenUsingRfcommWithServiceRecord("NAME", uuid);
            }
            catch (IOException e)
            {
            	e.printStackTrace();
            }
        }

        @Override
        public void run()
        {
        	BluetoothSocket tmpBluetoothSocket = null;
        	
        	Log.d(TAG, "AcceptThread running ... ");
        	
        	// Loop until a non-null socket is returned or an exception occurs.
        	while(tmpBluetoothSocket == null)
        	{
        		try
        		{
        			tmpBluetoothSocket = localServerSocket.accept(5000);
        		}
        		catch (Exception e)
        		{
        			Log.e(TAG, "AcceptThread Failed.", e);
        			
        			// Kill loop on Exception
        			break;
        		}
        	}
        	
        	connected(tmpBluetoothSocket);
        }

        public void cancel()
        {
            try
            {
            	localServerSocket.close();
            }
            catch(IOException e)
            {
            	Log.e(TAG, "Accept.cancel()", e);
            }
        }
    }

    

    private class ConnectThread extends Thread
    {
    	private BluetoothSocket mTmpBluetoothSocket;
    	
        public ConnectThread(BluetoothDevice device)
        {
        	super("Phone ConnectThread");
        	
            try
            {
            	mTmpBluetoothSocket = device.createRfcommSocketToServiceRecord(uuid);
            }
            catch (IOException e )
            {
            	e.printStackTrace();
            }
        }

        public void run()
        {
        	// Cancel discovery because it will slow down the connection
            mBluetoothAdapter.cancelDiscovery();
            
            try
            {
            	mTmpBluetoothSocket.connect();
            }
            catch (Exception e)
            {
            	Log.e(TAG, "ConnectThread run() failure:", e);
            	FileUtils.closeCloseable(mTmpBluetoothSocket);
            	mTmpBluetoothSocket = null;
            }
            finally
        	{
        		connected(mTmpBluetoothSocket);
        	}
        }
        
        public void cancel()
        {
        	try
			{
				mTmpBluetoothSocket.close();
			}
        	catch (IOException e)
			{
				e.printStackTrace();
			}
        }
    }

    private static final int FILE_TRANSFER_BYTE = 1;
    // Ivan: In my experience this is unnecessary, because any socket reads will simply block if no more bytes are available.
    //private static final int PASSIVE_LISTENING_BYTE = 1;
    private static final int TIMESTAMP_BYTE = 2;
    private static final int START_PLAYING_BYTE = 3;
    private static final int PAUSE_BYTE = 4;
    private static final int RESUME_FROM_PAUSE_BYTE = 5;
    // Ivan: In my experience this is an inappropriate message, because it cannot be transmitted if the connection is broken.
    //private static final int CONNECTION_LOST_BYTE = 6;
    private static final int HAPTIC_FILE_TRANSFER_COMPLETE = 7;
    private static final int PLAYBACK_COMPLETE = 8;
    
    private static final int BUFFER_SIZE = 1024;

    private class ConnectionThread extends Thread
    {
        //private InputStream mInputStream;
        //private OutputStream mOutputStream;
    	
    	private BufferedInputStream mInputStream;
        private BufferedOutputStream mOutputStream;
        
       // private volatile boolean watchIsReady = false;

        public ConnectionThread ()
        {
        	super("Phone ConnectionThread");
        	
            try
            {    	
            	mInputStream = new BufferedInputStream(mBluetoothSocket.getInputStream(), BUFFER_SIZE);
            	mOutputStream = new BufferedOutputStream(mBluetoothSocket.getOutputStream(), BUFFER_SIZE);
            }
            catch(IOException e)
            {
            	Log.e(TAG, "ConnectionThread constructor failed.", e);
            }
        }


        public void run()
        {
        	BluetoothDevice btd = null;
        	
        	// Cache BluetoothDevice so it can be broadcasted when the connection closes.
        	synchronized(BluetoothCommunication.this)
        	{
        		if(mBluetoothSocket != null)
        		{
        			btd = mBluetoothSocket.getRemoteDevice();
        		}
        	}
        	
        	try
        	{
        		while(true)
        		{

        			// Read first byte from the watch, which will determine what we do next
        			final int firstByte = mInputStream.read();

        			switch (firstByte)
        			{
        				case HAPTIC_FILE_TRANSFER_COMPLETE:

        					Log.d(TAG, "HAPTIC_FILE_TRANSFER_COMPLETE");

        					//watchIsReady = true;
        					mHandler.postOnHapticUploadSuccess();

        					break;


        				default:

        					Log.e(TAG, "UNKNOWN BYTE READ!!!");

        					break;
        			}
        		}
        	}
        	catch (Exception e)
        	{
        		Log.e(TAG, "ConnectionThread.run() loop exception.", e);
        	}
        	finally
        	{
        		Log.w(TAG, "ConnectionThread finished.");
        		
        		closeBluetoothSocket();
        		mHandler.postOnDisconnected(btd);
        		
        		if(!isClosed)
            	{
            		startAcceptThread();
            	}
        	}	
        }
        
        
        
        public void cancel()
        {
            closeBluetoothSocket();
        }
        

        // Here we synchronize on the current instance of the connection thread
        // to ensure nothing is written into the BluetoothSocket's output stream while
        // sending the haptic file to the watch.
        public synchronized void sendHaptFile(File haptFile)
        {
        	final byte[] buffer = new byte[BUFFER_SIZE];

        	BufferedInputStream bis = null;    
        	try
        	{
        		Log.d(TAG, "Haptic file size = " + haptFile.length());

        		final ByteBuffer buf = ByteBuffer.allocate(9);
        		buf.put((byte)FILE_TRANSFER_BYTE);
        		buf.putLong(haptFile.length());

        		// tell device to prepare to receive file
        		write(buf.array());

        		bis = new BufferedInputStream(new FileInputStream(haptFile), BUFFER_SIZE);

        		int len;
        		int totalBytesWritten = 0;

        		while ((len = bis.read(buffer)) != -1)
        		{
        			Log.e(TAG, "Writing " + len + " bytes");

        			totalBytesWritten += len;

        			write(buffer, 0, len);
        		}

        		Log.d(TAG, "Done writing total bytes written = " + totalBytesWritten);
        	}
        	catch (IOException e)
        	{
        		Log.e(TAG, "sendHapticFile() failed", e);
        		mHandler.postOnHapticUploadFailure();
        	}
        	finally
        	{
        		FileUtils.closeCloseable(bis);
        	}
        }

        public void sendTimeStamp(int timestamp)
        {
            try
            {
                final ByteBuffer buffer = ByteBuffer.allocate(5);
                buffer.put((byte)TIMESTAMP_BYTE);
                buffer.putInt(timestamp);
                
                final byte[] tmp = buffer.array();
                
                Log.d(TAG, "********************");
                for(int i = 0; i < 5; ++i)
                {
                	Log.d(TAG, "Sending byte = " + tmp[i]);
                }

                //write(buffer.array());
                write(tmp);
            }
            catch(IOException e)
            {
            	Log.e(TAG, "sendVideoViewTimestamp() failed", e);
            }
        }
        

		public void startHapticPlayback(int timestamp)
		{
			try
            {
                final ByteBuffer buffer = ByteBuffer.allocate(5);
                buffer.put((byte)START_PLAYING_BYTE);
                buffer.putInt(timestamp);

                write(buffer.array());
            }
            catch(IOException e)
            {
            	Log.e(TAG, "startHapticPlayback() failed", e);
            }
		}
        

        /** 
         * Added to ensure calls to the {@link BluetoothSocket}'s {@link OutputStream} are synchronized so there is
         * no mashing of data.
         * 
         * @param buffer -- byte[]
         * @param offset -- Offset into buffer
         * @param len -- Number of bytes to send from the buffer starting at offset.
         * @throws IOException
         */
        public synchronized void write(final byte[] buffer, final int offset, final int len) throws IOException
        {
        	mOutputStream.write(buffer, offset, len);
        	mOutputStream.flush();
        }
        
        public void write(byte[] buffer) throws IOException
        {
            write(buffer, 0, buffer.length);
        }
    }

    /*
    public synchronized boolean watchIsReady()
    {
    	if(mConnectionThread != null)
    	{
    		return mConnectionThread.watchIsReady;
    	}
    	
    	return false;
    }
    */
    
    public synchronized void sendResumeFromPauseEvent(int bufferingPosition, int requestId)
    {
        if (isConnected())
        {
            try
			{
				mConnectionThread.write( new byte[] {RESUME_FROM_PAUSE_BYTE} );
				//mConnectionThread.write( new byte[] {PASSIVE_LISTENING_BYTE} );
			}
            catch (IOException e)
			{
				e.printStackTrace();
			}
        }
    }

    
    
    public void sendHaptFile(final File haptFile)
    {
        if (isConnected() && mConnectionThread != null)
        {
            mConnectionThread.sendHaptFile(haptFile);
        }
    }

    
    
    public synchronized void startHapticPlayback(int timestamp)
    {
        if (mConnectionThread != null)
        {
        	Log.d(TAG, "Sending START_PLAYING_BYTE");
            mConnectionThread.startHapticPlayback(timestamp);
        }
    }
    
   
    public synchronized void sendTimeStamp(int timestamp)
    {
        if (mConnectionThread != null)
        {
            mConnectionThread.sendTimeStamp(timestamp);
        }
    }

    
    public synchronized void write(byte[] buffer)
    {
    	if (mConnectionThread != null)
        {
            try
			{
				mConnectionThread.write(buffer);
			}
            catch (IOException e)
			{
				e.printStackTrace();
			}
        }
    }
    
    public synchronized void onPlaybackComplete()
    {
    	if (mConnectionThread != null)
        {
            try
			{
				mConnectionThread.write(new byte[] { PLAYBACK_COMPLETE });
			}
            catch (IOException e)
			{
            	Log.e(TAG, "onPlaybackComplete() failed", e);
			}
        }
    }
    

    public synchronized void sendPauseEvent()
    {
    	if (mConnectionThread != null)
        {
            try
			{
				mConnectionThread.write(new byte[] { PAUSE_BYTE });
			}
            catch (IOException e)
			{
            	Log.e(TAG, "sendPauseEvent() failed", e);
			}
        }
    }
    
    
    public synchronized void startHapticPlayback()
    {
    	if (mConnectionThread != null)
        {
            try
			{
				mConnectionThread.write( new byte[] {START_PLAYING_BYTE} );
				Log.d(TAG, "startHapticPlayback() success.");
			}
            catch (IOException e)
			{
				Log.e(TAG, "startHapticPlayback() failed", e);
			}
        }
    }
    
    
    public void queueToastMessage(final String message)
    {
    	mHandler.post(new ToastRunnable(message));
    }
    
    private class ToastRunnable implements Runnable
    {
    	private String message;
    	
    	public ToastRunnable(final String msg)
    	{
    		message = msg;
    	}
    	
    	@Override
    	public void run()
    	{
    		Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    	}
    }
    
    public void setBluetoothUICallback(final BluetoothUICallback bluetoothCallback)
    {
    	mHandler.setBluetoothUICallback(bluetoothCallback);
    }
    
    public void removeBluetoothUICallback(final BluetoothUICallback bluetoothCallback)
    {
    	mHandler.removeBluetoothUICallback(bluetoothCallback);
    }
    
    private static class BluetoothUICallbackHandler extends Handler
    {
    	private List<BluetoothUICallback> mBluetoothUICallbacks = new ArrayList<BluetoothUICallback>(10);
    	
    	private static final int ON_CONNECTING = 0;
    	private static final int ON_CONNECTED = 1;
    	private static final int ON_DISCONNECTED = 2;
    	private static final int ON_CONNECTION_FAILURE = 3;
    	private static final int ON_HAPTIC_FILE_UPLOAD_SUCCESS = 4;
    	private static final int ON_HAPTIC_FILE_UPLOAD_FAILURE = 5;
    	
    	private static final String BLUETOOTH_DEVICE = "BLUETOOTH_DEVICE";
    	
    	private Context mContext;
    	
    	public BluetoothUICallbackHandler(final Context context)
    	{
    		mContext = context;
    	}
    	
    	@Override
        public void handleMessage(final Message msg)
        {
           
            switch (msg.what)
            {
            	case ON_CONNECTING:
            		handleOnConnecting();
            		break;
            		
            	case ON_CONNECTED:
            		handleOnConnectedMessage((BluetoothDevice)msg.getData().getParcelable(BLUETOOTH_DEVICE));
            		break;
            	
            	case ON_DISCONNECTED:
            		handleOnDisconnectedMessage((BluetoothDevice)msg.getData().getParcelable(BLUETOOTH_DEVICE));
            		break;
            		
            	case ON_CONNECTION_FAILURE:
            		handleOnConnectionFailureMessage();
            		break;
            		
            	case ON_HAPTIC_FILE_UPLOAD_SUCCESS:
            		handleOnHapticFileUploadSuccessMessage();
            		break;
            		
            	case ON_HAPTIC_FILE_UPLOAD_FAILURE:
            		handleOnHapticFileUploadFailureMessage();
            		break;
            }
        }
    	
    	public void postOnConnecting()
    	{
    		sendMessage(obtainMessage(ON_CONNECTING));
    	}
    	
    	public void postOnConnected(final BluetoothDevice btd)
    	{
    		final Bundle b = new Bundle();
    		b.putParcelable(BLUETOOTH_DEVICE, btd);
    		
    		final Message msg = obtainMessage(ON_CONNECTED);
    		msg.setData(b);
    		
    		sendMessage(msg);
    	}
    	
    	public void postOnDisconnected(final BluetoothDevice btd)
    	{
    		final Bundle b = new Bundle();
    		b.putParcelable(BLUETOOTH_DEVICE, btd);
    		
    		final Message msg = obtainMessage(ON_DISCONNECTED);
    		msg.setData(b);
    		
    		sendMessage(msg);
    	}
    	
    	public void postOnConnectionFailure()
    	{
    		sendMessage(obtainMessage(ON_CONNECTION_FAILURE));
    	}
    	
    	public void postOnHapticUploadSuccess()
    	{
    		sendMessage(obtainMessage(ON_HAPTIC_FILE_UPLOAD_SUCCESS));
    	}
    	
    	public void postOnHapticUploadFailure()
    	{
    		sendMessage(obtainMessage(ON_HAPTIC_FILE_UPLOAD_FAILURE));
    	}
    	
    	public synchronized void setBluetoothUICallback(final BluetoothUICallback bluetoothCallback)
    	{
    		mBluetoothUICallbacks.add(bluetoothCallback);
    	}
    	
    	public synchronized void removeBluetoothUICallback(final BluetoothUICallback bluetoothCallback)
    	{
    		mBluetoothUICallbacks.remove(bluetoothCallback);
    	}
    	
    	// private message handler methods
    	
    	private synchronized void handleOnConnecting()
    	{
    		for(final BluetoothUICallback btc : mBluetoothUICallbacks)
    		{
    			btc.onConnecting();
    		}
    	}
    	
    	private synchronized void handleOnConnectedMessage(final BluetoothDevice btd)
    	{
    		try
    		{
    			for(final BluetoothUICallback btc : mBluetoothUICallbacks)
    			{
    				btc.onConnected(btd);
    			}
    		}
    		catch(Exception e)
    		{
    			Log.e(TAG, "handleOnConnectedMessage failed", e);
    		}
    	}
    	
    	private synchronized void handleOnDisconnectedMessage(final BluetoothDevice btd)
    	{
    		for(final BluetoothUICallback btc : mBluetoothUICallbacks)
    		{
    			btc.onDisconnected(btd);
    		}
    	}
    	
    	private synchronized void handleOnConnectionFailureMessage()
    	{
    		for(final BluetoothUICallback btc : mBluetoothUICallbacks)
    		{
    			btc.onConnectionFailure();
    		}
    	}
    	
    	private synchronized void handleOnHapticFileUploadSuccessMessage()
    	{
    		Toast.makeText(mContext, "Successfully uploaded the haptic file.", Toast.LENGTH_SHORT).show();
    		
    		for(final BluetoothUICallback btc : mBluetoothUICallbacks)
    		{
    			btc.onHapticFileUploadSuccess();
    		}
    	}
    	
    	private synchronized void handleOnHapticFileUploadFailureMessage()
    	{
    		Toast.makeText(mContext, "Failed to upload the haptic file.", Toast.LENGTH_SHORT).show();
    		
    		for(final BluetoothUICallback btc : mBluetoothUICallbacks)
    		{
    			btc.onHapticFileUploadFailure();
    		}
    	}
    }
    
    public interface BluetoothUICallback
    {
    	public void onConnecting();
    	public void onConnectionFailure();
    	public void onConnected(BluetoothDevice bluetoothDevice);
    	public void onDisconnected(BluetoothDevice bluetoothDevice);
    	public void onHapticFileUploadSuccess();
    	public void onHapticFileUploadFailure();
    }
}

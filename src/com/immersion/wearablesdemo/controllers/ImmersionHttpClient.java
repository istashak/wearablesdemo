package com.immersion.wearablesdemo.controllers;

import android.util.Log;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.Map;

public class ImmersionHttpClient
{

    private static String TAG = "ImmersionHttpClient";

    private DefaultHttpClient mHttpClient = null;

    public static ImmersionHttpClient getHttpClient()
    {
        final ImmersionHttpClient httpClient = new ImmersionHttpClient();

        httpClient.setScheme();

        return httpClient;
    }

    private ImmersionHttpClient()
    {
    }

    private void setScheme()
    {
        if (this.mHttpClient == null)
        {
            final HttpParams httpParameters = new BasicHttpParams();
            ConnManagerParams.setMaxTotalConnections(httpParameters, 5);
            HttpConnectionParams.setConnectionTimeout(httpParameters, 5000); // milliseconds

            final SchemeRegistry schemeRegistry = new SchemeRegistry();

            schemeRegistry.register(new Scheme("http", PlainSocketFactory
                    .getSocketFactory(), 80));
            schemeRegistry.register(new Scheme("https", SSLSocketFactory
                    .getSocketFactory(), 443));

            final ClientConnectionManager cm = new ThreadSafeClientConnManager(
                    httpParameters, schemeRegistry);
            mHttpClient = new DefaultHttpClient(cm, httpParameters);
        }
    }

    public HttpResponse executeGet(final String requestString,
            final Map<String, String> headers, final int timeout)
            throws Exception
    {
        HttpResponse resp = null;

        resp = execute(new HttpGet(requestString), headers, timeout);

        return resp;
    }

    public HttpResponse executePost(final String requestString,
            final Map<String, String> headers, final int timeout)
            throws Exception
    {
        return execute(new HttpPost(requestString), headers, timeout);
    }

    public HttpResponse executeDelete(final String requestString,
            final Map<String, String> headers, final int timeout)
            throws Exception
    {
        return execute(new HttpDelete(requestString), headers, timeout);
    }

    public HttpResponse executePostWithBody(final String requestString,
            final String body, final Map<String, String> headers,
            final int timeout) throws Exception
    {
        HttpPost request = new HttpPost(requestString);

        StringEntity se;
        try
        {
            se = new StringEntity(body, HTTP.UTF_8);
        } catch (UnsupportedEncodingException e)
        {
            throw e;
        }

        // se.setContentType("application/x-www-form-urlencoded");
        request.setEntity(se);

        return execute(request, headers, timeout);
    }

    private HttpResponse execute(final HttpUriRequest request,
            final Map<String, String> headers, final int timeout)
            throws Exception
    {
        /*
        try
        {
        */
            final URI uri = request.getURI();

            final String host = uri.getHost() != null ? uri.getHost().trim() : "";
            
            if (host.length() > 0)
            {
                request.setHeader("Host", host);
            }

            // set custom headers, possibly overriding default headers
            if (headers != null)
            {
                for (Map.Entry<String, String> hdr : headers.entrySet())
                {
                    request.setHeader(hdr.getKey(), hdr.getValue());
                }
            }

            if (true)
            {
                Header[] hdrs = request.getAllHeaders();
                Log.i(TAG, "request URI [" + request.getURI() + "]");
                for (Header hdr : hdrs)
                {
                    Log.i(TAG, "request header [" + hdr.toString() + "]");
                }
            }

            HttpConnectionParams.setSoTimeout(mHttpClient.getParams(), timeout);
            final HttpResponse resp = mHttpClient.execute(request);

            if (resp == null)
            {
                throw new RuntimeException("Null response returned.");
            }

            return resp;
            /*
        }
        catch (SocketTimeoutException e)
        {
            throw e;
        }
        catch (UnknownHostException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw e;
        }
        */
    }

    public HttpParams getParams()
    {
        return mHttpClient.getParams();
    }
}

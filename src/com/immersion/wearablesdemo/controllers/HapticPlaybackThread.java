package com.immersion.wearablesdemo.controllers;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import android.widget.MediaController;

import com.immersion.content.EndpointWarp;
import com.immersion.wearablesdemo.models.HapticFileInformation;
import com.immersion.wearablesdemo.models.NotEnoughHapticBytesAvailableException;
import com.immersion.wearablesdemo.utils.Profiler;
import com.immersion.wearablesdemo.utils.RuntimeInfo;

/**
 * The HapticPlaybackThread coordinates communication between the portion of the
 * UI thread controlling media playback, and the background threads enabling
 * haptic progressive playback in order to facilitate synchronization of haptic
 * playback with AV playback. More specifically, the HapticPlaybackThread is a
 * background thread exposing a {@link Handler} for being looped on by a
 * {@link Looper}. The {@link Handler} allows asynchronous communication between
 * the UI thread and itself; and asynchronous communication between a
 * {@link HapticDownloadThread} and itself.
 *
 *
 * @author ivan stashak
 *
 */
public class HapticPlaybackThread extends Thread
{
    private static final String TAG = "HapticPlaybackThread";

    public static final int HAPTIC_SET_BUFFERING_POSITION = 1;
    public static final int HAPTIC_PLAYBACK_FOR_TIME_CODE = 2;
    public static final int HAPTIC_BYTES_AVAILABLE_TO_DOWNLOAD = 3;
    public static final int HAPTIC_STOP_PLAYBACK = 4;
    public static final int HAPTIC_PLAYBACK_IS_READY = 5;

    public static final int PAUSE_AV_FOR_HAPTIC_BUFFERING = 6;

    public static final int HAPTIC_DOWNLOAD_ERROR = 7;
    public static final String HAPTIC_DOWNLOAD_EXCEPTION_KEY = "haptic_download_exception";

    public static final int HAPTIC_BUFFER_UP_TIMEOUT = 8;

    public static final int AV_SEEK_TO = 9;

    private static final String PLAYBACK_TIMECODE_KEY = "playback_timecode";
    private static final String PLAYBACK_UPTIME_KEY = "playback_uptime";

    /**
     * A millisecond value used to increment the synchronization variable
     * {@code mHapticPlaytimeElapsed} on every pass through the {@code mPlaybackBufferUpRunnable}.
     *
     * Note that this value must be a multiple of the sample duration (for a 200Hz signal, this must
     * a multiple of 5)!
     */
    private static final int HAPTIC_DATA_PUSH_INTERVAL = 40;

    private final String mHapticUri;
    private Handler mHandler;
    private final Handler mUIHandler;

    private HapticDownloadThread mHapticDownloadThread;

    private Looper mLooper;

    private MediaController.MediaPlayerControl mMediaPlayerController;

    private IHapticFileReader mHapticFileReader;

    /* Uncomment code to re-enable host device haptics
    private EndpointWarp mEndpointWarp;
	*/
    
    private final Profiler mProfiler = new Profiler();

    /**
     * The AV playback position in milliseconds being requested to sync haptic with.  Care should be taken
     * to ensure that this position in not only at the beginning of an AV frame, but also at the beginning
     * of a haptic frame.  Currently only one channel is used so alignment is somewhat trivial.  This will change
     * when multiple channels are added.
     */
    private int mBufferedPlaybackPosition;

    /**
     * This is the id of the request to buffer at a given AV playback position, determined by {@code mBufferedPlaybackPosition}.  This
     * id will be passed back to the UI layer along with the value of {@code mBufferedPlaybackPosition} when buffering is complete and
     * playback is ready to begin.
     */
    private int mRequestId;

    /**
     * Specifies the number of times buffering is attempted at the playback position specified by {@code mBufferedPlaybackPosition} before the UI layer is
     * passed a HAPTIC_BUFFER_UP_TIMEOUT message allowing the user to quit or try again.
     */
    private int mBufferAttemptCount;

    /**
     * Holds the value of {@code SystemClock.uptimeMillis()} when playback is requested to start by calling the function
     * {@code playHapticForPlaybackPosition(final int currentPlaybackPositionMS, final long uptimeMillis)}.  This variable
     * is used for synchronization purposes.
     */
    private long mUptimeOnPlaybackStart;

    /**
     * Holds the current AV playback position in milliseconds when playback is requested to start by calling the function
     * {@code playHapticForPlaybackPosition(final int currentPlaybackPositionMS, final long uptimeMillis)}.  This variable
     * is used for synchronization purposes.
     *
     * <p>
     * NOTE: This variables value should always be equal to {@code mBufferedPlaybackPosition}'s value.
     * </p>
     */
    private int mPlayedPlaybackPosition;

    /**
     * This variable tracks amount of time that shoul have theoretically elapsed since the last call to the function
     * {@code playHapticForPlaybackPosition(final int currentPlaybackPositionMS, final long uptimeMillis)}.  It is
     * used for synchronization purposes.  This value added to {@code mUptimeOnPlaybackStart} is when the next {@link PlaybackRunnable}
     * is scheduled to run via the {@link Handler}'s {@code postDelayed(Runnable r, long delayMillis)} function.
     *
     * <p>
     * Currently this value is monotonically incremented by the value the static variable {@code HAPTIC_DATA_PUSH_INTERVAL}.
     * </p>
     */
    private int mHapticPlaytimeElapsed;


    private boolean mDownloadError = false;
    private boolean mRunning = false;

    Context mContext;

    /**
     * This variable determines the first pass through of a buffer up and haptic playback after a call
     * to {@code playHapticForPlaybackPosition(final int currentPlaybackPositionMS, final long uptimeMillis)}.
     * It allows the {@link HapticPlaybackThread} to tell the UI thread to seek to the requested playback position
     * it should already be at ... and is essentially a hack in an attempt to make the two file solution work!
     */
    private boolean mIsFirstTime;
    private RuntimeInfo mInfo;
    private boolean mIsStreaming = false;

    public HapticPlaybackThread(final Context context, final String hapticUri, final Handler uiHandler, final boolean isStreaming)
    {
        super("HapticPlaybackThread");

        mHapticUri = hapticUri;
        mUIHandler = uiHandler;
        mContext = context;
        mIsStreaming = isStreaming;
    }

    @Override
    public void run()
    {
        Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO);

        Looper.prepare();

        mLooper = Looper.myLooper();

        mHandler = new MyHandler();

        mHapticFileReader = new MemoryMappedFileReader();
        mHapticFileReader.setBlockSizeMS(HAPTIC_DATA_PUSH_INTERVAL);

        mHapticDownloadThread = new HapticDownloadThread(mHapticUri, mHandler, mIsStreaming);
        mHapticDownloadThread.start();

        Looper.loop();
    }

    /**
     * Ends the {@link HapticPlaybackThread}'s {@link Looper}, killing the
     * thread, and releasing other important resources.
     */
    public void quit()
    {
        if (mLooper != null)
        {
            mLooper.quit();
        }

        if (mHapticFileReader != null)
        {
            mHapticFileReader.close();
        }

        
        
        /* Uncomment code to re-enable host device haptics
        // Kill the Warp thread
        if (mEndpointWarp != null)
        {
            mEndpointWarp.stop();
            mEndpointWarp.dispose();
            mEndpointWarp = null;
        }
        */
    }

    /**
     * Posts a message to this thread's handler telling it to start buffering
     * haptic data for the current AV position. This function is meant to be
     * called from the UI thread.
     *
     * @param currentPlaybackPosition
     *            -- The current AV playback position in milliseconds.
     */
    public void prepareHapticPlayback(final int currentPlaybackPositionMS, final int requestId)
    {
        mHandler.removeMessages(HAPTIC_SET_BUFFERING_POSITION);
        mHandler.sendMessage(mHandler.obtainMessage(HAPTIC_SET_BUFFERING_POSITION, currentPlaybackPositionMS, requestId));
    }

    /**
     * Posts a message to this thread's handler telling it to get haptic data
     * for the current AV playback position. This function is meant to be called
     * from the UI thread.
     *
     * @param currentPlaybackPositionMS
     *            -- The current AV playback position in milliseconds.
     */
    public void playHapticForPlaybackPosition(final int currentPlaybackPositionMS, final long uptimeMillis)
    {
        mHandler.removeMessages(HAPTIC_PLAYBACK_FOR_TIME_CODE);

        final Bundle b = new Bundle();

        b.putInt(PLAYBACK_TIMECODE_KEY, currentPlaybackPositionMS);
        b.putLong(PLAYBACK_UPTIME_KEY, uptimeMillis);

        final Message msg = mHandler.obtainMessage(HAPTIC_PLAYBACK_FOR_TIME_CODE);
        msg.setData(b);

        mHandler.sendMessage(msg);
    }

    /**
     * Posts a message to this thread's handler telling it to stop haptic
     * playback. This function is meant to be called from the UI thread.
     */
    public void stopHapticPlayback()
    {
        mHandler.sendEmptyMessage(HAPTIC_STOP_PLAYBACK);
    }

    private class MyHandler extends Handler
    {

        @Override
        public void handleMessage(final Message msg)
        {
            switch (msg.what)
            {
            case HAPTIC_SET_BUFFERING_POSITION:

                mHandler.removeCallbacks(mSetBufferPositionRunnable);
                mBufferedPlaybackPosition = msg.arg1;
                mRequestId = msg.arg2;
                mBufferAttemptCount = 0;
                handleSetBufferPosition();

                break;

            case HAPTIC_PLAYBACK_FOR_TIME_CODE:

                final Bundle b = msg.getData();

                handlePlaybackForTimeCode(b.getInt(PLAYBACK_TIMECODE_KEY), b.getLong(PLAYBACK_UPTIME_KEY));

                break;

            case HAPTIC_BYTES_AVAILABLE_TO_DOWNLOAD:

                mHapticFileReader.setBytesAvailable(msg.arg1);

                break;

            case HAPTIC_STOP_PLAYBACK:

                handleStopHapticPlayback();

                break;

            case HAPTIC_DOWNLOAD_ERROR:

                handleHapticDownloadError(msg);

                break;

            }
        }
    }

    private void handleHapticDownloadError(final Message msg)
    {
        mDownloadError = true;

        final Message newMessage = mUIHandler.obtainMessage(HAPTIC_DOWNLOAD_ERROR);
        newMessage.setData(msg.getData());

        mUIHandler.sendMessage(newMessage);
    }

    private static final long BUFFER_WAIT_TIME_MS = 100l;
    private static final int ALLOWED_BUFFER_ATTEMPTS = 3000;

    private void handleSetBufferPosition()
    {
        if (!mDownloadError)
        {
            if (mBufferAttemptCount++ >= ALLOWED_BUFFER_ATTEMPTS)
            {
                mUIHandler.sendMessage(mUIHandler.obtainMessage(HAPTIC_BUFFER_UP_TIMEOUT));
            }
            else if (mHapticFileReader.bufferAtPlaybackPosition(mBufferedPlaybackPosition))
            {
                mUIHandler.sendMessage(mUIHandler.obtainMessage(HAPTIC_PLAYBACK_IS_READY, mBufferedPlaybackPosition, mRequestId));
            }
            else
            {
                mHandler.postDelayed(mSetBufferPositionRunnable, BUFFER_WAIT_TIME_MS);
            }
        }
    }

    private final Runnable mSetBufferPositionRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            handleSetBufferPosition();
        }
    };

    private void handleStopHapticPlayback()
    {
        mRunning = false;

        /* Uncomment code to re-enable host device haptics
        if (mEndpointWarp != null)
        {
            mEndpointWarp.stop();
        }
        */

        // Remove any pending callback runnables from the queue
        mHandler.removeCallbacks(mSetBufferPositionRunnable);
        mHandler.removeCallbacks(mPlaybackBufferUpRunnable);

        for (final PlaybackRunnable pbr : mPlaybackRunnables)
        {
            mHandler.removeCallbacks(pbr);
        }

        mPlaybackRunnables.clear();
    }

    private void handlePlaybackForTimeCode(final int currentPlaybackPosition, final long uptimeOnPlaybackStart)
    {
        // Warp thread should follow Runnable behavior
    	/* Uncomment code to re-enable host device haptics
        if (!mRunning)
        {
            HapticFileInformation hfi = mHapticFileReader.getHapticFileInformation();

            // TODO: Should store all this in the correct types inside
            // HapticFileInformation rather than casting here.
            byte major = (byte) hfi.getMajorVersion();
            byte minor = (byte) hfi.getMinorVersion();
            // byte encoding = (byte) hfi.getEncoding();
            byte encoding = 2; // FOR NOW... encoding isn't set in the files on
                               // the server, so assume it's time-ordered
                               // amplitudes
            int sampleRate = hfi.getSampleHertz();
            short bps = (short) hfi.getBitsPerSample();
            byte numChannels = (byte) hfi.getNumberOfChannels();
            byte actuators[] = new byte[numChannels];
            for (int i = 0; i < numChannels; i++)
            {
                actuators[i] = (byte) hfi.getActuatorArray()[i];
            }
            byte compression = (byte) hfi.getCompressionScheme();

            mEndpointWarp = new EndpointWarp(mContext, // context
                    major, // major
                    minor, // minor
                    encoding, // encoding
                    (byte) 0, // reserved
                    sampleRate, // sampleRate
                    bps, // bitPerSample
                    numChannels, // numChannels
                    actuators, // actuators[]
                    compression // compression
            );

            mEndpointWarp.start();
        }
		*/
    	
        mRunning = true;
        mHapticPlaytimeElapsed = 0;
        mPlayedPlaybackPosition = currentPlaybackPosition;
        mUptimeOnPlaybackStart = uptimeOnPlaybackStart;

        //Log.d(TAG, "handlePlaybackForTimeCode mPlayedPlaybackPosition = " + mPlayedPlaybackPosition);
        //Log.d(TAG,"handlePlaybackForTimeCode mUptimeOnPlaybackStart = " + mUptimeOnPlaybackStart);
        // Log.d(TAG, "handlePlaybackForTimeCode SystemClock.uptimeMillis() = "
        // + SystemClock.uptimeMillis());

        mIsFirstTime = true;

        playbackBufferUp();

        //Log.d(TAG, ".............. handlePlaybackForTimeCode() end ..............");
    }

    private void playbackBufferUp()
    {
        if (mRunning)
        {
            final int currentPlaybackPosition = mPlayedPlaybackPosition + mHapticPlaytimeElapsed;

            byte[] hapticPlaybackData;

            // Log.d(TAG, ">>>>>>>>>>>>>> playbackBufferUp(" +
            // currentPlaybackPosition + ") >>>>>>>>>>>>>>>");

            try
            {
                hapticPlaybackData = mHapticFileReader.getBufferForPlaybackPosition(currentPlaybackPosition);
            } catch (NotEnoughHapticBytesAvailableException e)
            {
                mRunning = false;

                Log.i(TAG, e.getMessage(), e);
                mUIHandler.sendMessage(mUIHandler.obtainMessage(PAUSE_AV_FOR_HAPTIC_BUFFERING, currentPlaybackPosition, 0));

                return;
            }

            if (hapticPlaybackData != null)
            {
                // Log.d(TAG, "mHapticPlaytimeElapsed = " +
                // mHapticPlaytimeElapsed);
                // Log.d(TAG, "Playback real elapsed time = " +
                // mProfiler.getDuration());
                // Log.d(TAG, "Round trip = " + mProfiler.getDurationII());
                // Log.d(TAG,
                // "######################################################");

                final long presentationTimestamp = mUptimeOnPlaybackStart + mHapticPlaytimeElapsed;

                final PlaybackRunnable playbackRunnable = new PlaybackRunnable(presentationTimestamp, hapticPlaybackData);
                mPlaybackRunnables.add(playbackRunnable);

                mHandler.postAtTime(playbackRunnable, presentationTimestamp);

                /*
                if(mIsFirstTime)
                {
                    mUIHandler.sendMessage(mUIHandler.obtainMessage(AV_SEEK_TO, mPlayedPlaybackPosition, 0));
                    mIsFirstTime = false;
                }
                */

                mHapticPlaytimeElapsed += HAPTIC_DATA_PUSH_INTERVAL;

                mProfiler.startTimingII();
            } else
            {
                Log.d(TAG, "Total haptic playback dration = " + mProfiler.getDuration());
                Log.d(TAG, "On playback end mHapticPlaytimeElapsed = " + mHapticPlaytimeElapsed);
                Log.d(TAG, "Thread priority = " + Thread.currentThread().getPriority());

                mRunning = false;
            }
        }
    }

    private final Runnable mPlaybackBufferUpRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            playbackBufferUp();
        }
    };

    private final List<PlaybackRunnable> mPlaybackRunnables = new ArrayList<PlaybackRunnable>();

    private class PlaybackRunnable implements Runnable
    {
        private final long mPresentationTimestamp;
        private final byte[] mHapticPlaybackData;

        public PlaybackRunnable(final long presentationTimestamp, final byte[] playbackData)
        {
            mPresentationTimestamp = presentationTimestamp;
            mHapticPlaybackData = playbackData;
        }

        @Override
        public void run()
        {
            if (mRunning)
            {
               
            	/* Uncomment code to re-enable host device haptics
                if(RuntimeInfo.areHapticsEnabled(mContext)) {
                    mEndpointWarp.update(mHapticPlaybackData, mHapticPlaybackData.length, mPresentationTimestamp);
                }
                */
            	
                mHandler.post(mPlaybackBufferUpRunnable);
                mPlaybackRunnables.remove(0);

                //Log.d(TAG, "end point warp call duration = " + mProfiler.getDurationII());


                if(mIsFirstTime)
                {
                    mUIHandler.sendMessage(mUIHandler.obtainMessage(AV_SEEK_TO, mPlayedPlaybackPosition, 0));
                    mIsFirstTime = false;
                }


                //Log.d(TAG, "Presentation time = " + mPresentationTimestamp + " actual time = " + actualTime);
            }
        }
    };

    public void syncUpdate(int avNewPos, long uptimeNow, int requestID)
    {
        int expectedPos = mPlayedPlaybackPosition + mHapticPlaytimeElapsed;
        int delta = avNewPos - expectedPos;
        // Log.d(TAG, "##syncUpdate## mPlayedPlaybackPosition=" + mPlayedPlaybackPosition + ", mHapticPlaytimeElapsed=" + mHapticPlaytimeElapsed);

        if (50 < Math.abs(delta))
        {
            // Log.d(TAG, "##syncUpdate## seeking haptics " + delta);
            mHapticPlaytimeElapsed = avNewPos - mPlayedPlaybackPosition;
            mUptimeOnPlaybackStart -= delta;
            // Log.d(TAG, "##syncUpdate## mHapticPlaytimeElapsed=" + mHapticPlaytimeElapsed + ", mUptimeOnPlaybackStart=" + mUptimeOnPlaybackStart);

            prepareHapticPlayback(avNewPos, requestID);
        }
    }
}

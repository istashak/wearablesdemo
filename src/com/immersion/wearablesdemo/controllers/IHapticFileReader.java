package com.immersion.wearablesdemo.controllers;

import com.immersion.wearablesdemo.models.HapticFileInformation;
import com.immersion.wearablesdemo.models.NotEnoughHapticBytesAvailableException;

/**
 * Exposes an interface that will allow for many possible implementations of a
 * hatic file reader
 *
 * @author ivan stashak
 *
 */
public interface IHapticFileReader
{
    /**
     * Informs the haptic file reader of the desired block-size for haptic data
     *
     * @param blockSizeMS
     */
    public void setBlockSizeMS(int blockSizeMS);

    /**
     * Informs the haptic file reader of the bytes the have been written to
     * file, and are thus available for buffering and playback.
     *
     * @param bytesAvailable
     */
    public void setBytesAvailable(int bytesAvailable);

    /**
     * <p>
     * Set the position for haptic playback buffering based on the timecode of
     * AV playback.
     * </p>
     * <p>
     * NOTE: For optimal synchronization purposes, the caller must ensure that the parameter playbackPositionMS lies
     * at not only the beginning of an AV frame, but also a haptic data frame.  This is trivial when using a single
     * channel that has a single byte boundary, but this will not be the case with the introduction of multiple channels;
     * and varying sample rates.
     * </p>
     *
     * @param playbackPositionMS -- The AV playback position in milliseconds.
     *
     * @return True if buffering was successful, false otherwise.
     */
    public boolean bufferAtPlaybackPosition(int playbackPositionMS);


    /**
     * <p>
     * Tells the haptic file reader to return the bytes necessary for haptic
     * playback at the current AV playback position.
     * </p>
     * <p>
     * NOTE: A call to this function must be preceded by a call to {@code startBufferingAtPlaybackPosition(int playbackMS)}.
     * The resolution of the parameter playbackPositionMS gotten by calling
     * getCurrentPosition() on a {@link VideoView} or {@link MediaPlayer} object, is
     * not accurate enough to be used to access data directly out of the buffer in a random fashion. Thus, again,
     * only call this function after first calling {@code startBufferingAtPlaybackPosition(int playbackMS)}.
     * </p>
     *
     * @param playbackPositionMS -- The AV playback position in milliseconds.
     *
     * @return A byte buffer with the haptic playback data.
     */
    public byte[] getBufferForPlaybackPosition(int playbackPositionMS) throws NotEnoughHapticBytesAvailableException;


    /**
     *
     * @return A {@link HapticFileInformation} object possessing specifics about the current HAPT file
     * being streamed.
     */
    public HapticFileInformation getHapticFileInformation();

    /**
     * Close any important resources.
     */
    public void close();
}

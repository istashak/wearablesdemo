package com.immersion.wearablesdemo.controllers;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.util.Log;

import com.immersion.wearablesdemo.models.HttpUnsuccessfulException;
import com.immersion.wearablesdemo.utils.FileUtils;

import org.apache.http.HttpResponse;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.URI;

import static com.immersion.wearablesdemo.controllers.HapticPlaybackThread.HAPTIC_BYTES_AVAILABLE_TO_DOWNLOAD;
import static com.immersion.wearablesdemo.controllers.HapticPlaybackThread.HAPTIC_DOWNLOAD_ERROR;
import static com.immersion.wearablesdemo.controllers.HapticPlaybackThread.HAPTIC_DOWNLOAD_EXCEPTION_KEY;

/**
 * A background thread to deal with downloading a HAPT file through an HTTP
 * connection. This thread also informs a running {@link HapticPlaybackThread},
 * via a {@link Handler}, so that it knows the status of the download.
 * 
 * @author ivan stashak
 * 
 */
public class HapticDownloadThread extends Thread
{
    private static final String TAG = "HapticDownloadThread";

    private String mHapticUri;
    private Handler mHandler;
    private boolean mIsStreaming;

    public HapticDownloadThread(final String hapticUri, final Handler handler, final boolean isStreaming)
    {
        super("HapticDownloadThread");

        mHapticUri = hapticUri;
        mHandler = handler;
        mIsStreaming = isStreaming;
    }

    @Override
    public void run()
    {
        if(mIsStreaming)
        {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

            final ImmersionHttpClient ihc = ImmersionHttpClient.getHttpClient();

            try
            {
                final HttpResponse httpResponse = ihc.executeGet(mHapticUri, null, 60000);

                final int status = httpResponse.getStatusLine().getStatusCode();

                if (status == 200)
                {
                    writeToFile(httpResponse.getEntity().getContent(), Integer.parseInt(httpResponse.getFirstHeader("Content-Length").getValue()));
                }
                else
                {
                    final StringBuilder sb = new StringBuilder("HTTP STATUS CODE: ");
                    sb.append(status);

                    switch(status)
                    {
                        case 400:
                            sb.append(" Bad Request");
                            break;

                        case 403:
                            sb.append(" Forbidden");
                            break;

                        case 404:
                            sb.append(" Not Found");
                            break;

                        case 500:
                            sb.append(" Internal Server Error");
                            break;

                        case 502:
                            sb.append(" Bad Gateway");
                            break;

                        case 503:
                            sb.append(" Service Unavailable");
                            break;
                    }

                    throw new HttpUnsuccessfulException(status, sb.toString());
                }
            }
            catch (Exception e)
            {
                final Message msg = mHandler.obtainMessage(HAPTIC_DOWNLOAD_ERROR);

                final Bundle b = new Bundle();
                b.putSerializable(HAPTIC_DOWNLOAD_EXCEPTION_KEY, e);

                msg.setData(b);

                mHandler.sendMessage(msg);

                Log.e(TAG, e.getMessage(), e);
            }
        }
        else
        {
            // LOCAL PLAYBACK
            InputStream is = null;
            try {
                is = new FileInputStream(mHapticUri);
            } catch (FileNotFoundException e) {
                Log.e(TAG, "FileNotFoundException", e);
            }
            if(is != null)
                try {
                    writeToFile(is, is.available());
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }
    

    /**
     * This function writes to a file named "dat.hapt" from an
     * {@link InputStream} retrieved from an {@link HttpResponse}. If the file
     * already exists it is deleted and recreated. In addition, while looping
     * through the {@link InputStream} and writing to file, the function alerts
     * the {@link HapticPlaybackThread}, that created this instance of the
     * {@link HapticDownloadThread}, of it's download progress. This is done
     * asynchronously via the {@link Handler} owned by the
     * {@link HapticPlaybackThread} that was passed to the constructor on
     * instantiation.
     * 
     * @param isFrom
     * @param contentLength
     * @return
     * @throws IOException 
     */
    public boolean writeToFile(final InputStream isFrom, final int contentLength) throws IOException
    {
        BufferedInputStream from = null;
        // BufferedOutputStream to = null;
        RandomAccessFile to = null;

        final byte[] buf = new byte[4096];

        try
        {

            if (isFrom != null && contentLength > 0)
            {
                FileUtils.deleteHapticStorageFile();

                final File file = FileUtils.getHapticStorageFile();

                from = new BufferedInputStream(isFrom);
                
                to = new RandomAccessFile(file, "rw");

                int totalBytesRead = 0;
                int bytesRead = 0;

                boolean tmpTest = true;

                while ((bytesRead = from.read(buf, 0, 4096)) >= 0)
                {
                    to.write(buf, 0, bytesRead);
                    totalBytesRead += bytesRead;

                    // Here we tell the haptic file reader to read
                    mHandler.sendMessage(mHandler.obtainMessage(HAPTIC_BYTES_AVAILABLE_TO_DOWNLOAD, totalBytesRead, 0));


                    /*
                    try
                    { 
                        if(totalBytesRead >= 4096 * 3)
                        {
                            Log.d(TAG, "Entering test sleep.");
                            sleep(60000);
                        }
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                    */
                }

                Log.d(TAG, "!!!!!!!!!!!!! FILE DOWNLOAD COMPLETE !!!!!!!!!!!!!!");

                return true;
            }
        }
        finally
        {
            FileUtils.closeCloseable(from);
            FileUtils.closeCloseable(to);
        }

        return false;
    }

}

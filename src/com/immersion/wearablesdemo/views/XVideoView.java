package com.immersion.wearablesdemo.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.VideoView;

import com.immersion.wearablesdemo.R;
import com.immersion.wearablesdemo.controllers.BaseApplication;
import com.immersion.wearablesdemo.controllers.BluetoothCommunication;
import com.immersion.wearablesdemo.controllers.HapticPlaybackThread;
import com.immersion.wearablesdemo.models.HttpUnsuccessfulException;
import com.immersion.wearablesdemo.models.VideoData;
import com.immersion.wearablesdemo.utils.Profiler;
import com.immersion.wearablesdemo.utils.VideoDataHelper;

import static android.media.MediaPlayer.MEDIA_ERROR_IO;
import static android.media.MediaPlayer.MEDIA_ERROR_MALFORMED;
import static android.media.MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK;
import static android.media.MediaPlayer.MEDIA_ERROR_SERVER_DIED;
import static android.media.MediaPlayer.MEDIA_ERROR_TIMED_OUT;
import static android.media.MediaPlayer.MEDIA_ERROR_UNKNOWN;
import static android.media.MediaPlayer.MEDIA_ERROR_UNSUPPORTED;
import static android.media.MediaPlayer.MEDIA_INFO_BAD_INTERLEAVING;
import static android.media.MediaPlayer.MEDIA_INFO_BUFFERING_END;
import static android.media.MediaPlayer.MEDIA_INFO_BUFFERING_START;
import static android.media.MediaPlayer.MEDIA_INFO_METADATA_UPDATE;
import static android.media.MediaPlayer.MEDIA_INFO_NOT_SEEKABLE;
import static android.media.MediaPlayer.MEDIA_INFO_UNKNOWN;
import static android.media.MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START;
import static android.media.MediaPlayer.MEDIA_INFO_VIDEO_TRACK_LAGGING;
import static com.immersion.wearablesdemo.controllers.HapticPlaybackThread.AV_SEEK_TO;
import static com.immersion.wearablesdemo.controllers.HapticPlaybackThread.HAPTIC_BUFFER_UP_TIMEOUT;
import static com.immersion.wearablesdemo.controllers.HapticPlaybackThread.HAPTIC_DOWNLOAD_ERROR;
import static com.immersion.wearablesdemo.controllers.HapticPlaybackThread.HAPTIC_DOWNLOAD_EXCEPTION_KEY;
import static com.immersion.wearablesdemo.controllers.HapticPlaybackThread.HAPTIC_PLAYBACK_IS_READY;
import static com.immersion.wearablesdemo.controllers.HapticPlaybackThread.PAUSE_AV_FOR_HAPTIC_BUFFERING;

public class XVideoView extends FrameLayout implements OnSeekBarChangeListener,
        OnPreparedListener, OnBufferingUpdateListener,
        OnSeekCompleteListener, OnInfoListener, OnErrorListener,
        OnCompletionListener, MediaController.MediaPlayerControl,
        OnTouchListener, OnClickListener
{
    private static final String TAG = "XVideoView";

    /**
     * This variable allows turning off seeking by setting it to false. When set
     * false, the rewind and fast-forward buttons are hidden, and scrubbie
     * seeking is disabled.
     */
    private static final boolean ALLOW_SEEKING = true;

    private VideoView mVideoView;

    private ProgressBar mBufferingSpinner;

    private View mMediaControlsPanel;
    //private View mLogoImage;

    private TextView mConnectionStatuTextView;
    
    private TextView mDurationTextView;
    private TextView mTimeElapsedTextView;
    
    private ImageButton mHomeImageButton;
    private ImageButton mPausePlayImageButton;
    private ImageButton mNextImageButton;
    
    private TextView mNextCategoryTextView;
    private TextView mNextNameTextView;

    private SeekBar mScrubberSeekBar;

    /**
     * This is the max value for the player controls' {@link SeekBar} (i.e.
     * mScrubberBar).
     */
    private static int MAX = 1000;

    /**
     * A variable ranging from [0, MAX] representing the position of the seek bar's thumb icon on the screen.
     * If the variable is at 0 the thumb icon is all the way to the left, if at MAX, the thumb icon is all
     * the way to the right.
     */
    private int mSeekToPosition;
    private int mRewindFastForwardPosition;

    /**
     * This variable is set in the {@code onPrepared(MediaPlayer mp)} function once the duration of the AV track is known.
     * This variable is either 1/10 the total AV duration or 1 second, whichever is larger, and is used to increment or decrement
     * the AV playback position when the user pushes the fast forward or rewind button, respectively.
     */
    private int mRewindFastForwardDelta;

    /**
     * This variable tracks the most recently requested seekTo() buffer position.  This position
     * is placed at a timecode thought to be the beginning of an AV-frame.
     */
    private int mRequestedBufferPosition;
    
    /**
     * This variable tracks the most recent request to buffer the haptic data at an AV timecode position.  This
     * variable will be compared against
     */
    private int mBufferRequestId;

    private Handler mHandler;
    private HapticPlaybackThread mHapticPlaybackThread;

    /**
     * Used to determine if playback was happening when the user used the
     * scrubber to seek.
     */
    private boolean mWasPlaying;
    
    /**
     * Tracks whether the user is seeking by dragging the scrubbie.  It helps
     * safeguard against playback being started when the user is seeking.  See the 
     * {@code handleMessage()} function in the private class {@code MyUIHandler} for
     * more details. 
     */
    private boolean mIsSeeking = false;

    /**
     * This variable is used to indicate whether the AV-haptic playback has been requested to start.
     * Tracking this state is necessary since starting of AV-Haptic playback is an asynchronous process with some latency where
     * things like user button pushes can happen in between.
     */
    private boolean mIsStarting = false;

    /**
     * Tracks whether the {@link MediaPlayer} controlled by the
     * mMediaPlayerControl member is prepared and ready for playback.
     */
    private boolean mIsPrepared = false;
    
    /**
     * Tracks whether or not the user requested to pause while the AV was preparing.  If set true, playback will not be started
     * when the {@code onPrepared(MediaPlayer mp)} function is hit.
     */
    private boolean mWasPausedWhenNotPrepared = false;
    
    /**
     * Tracks if either AV or haptic is buffering.
     */
    private boolean mIsBuffering = false;
    
    //private volatile boolean mIsInitialized = false;
    
    /**
     * Tracks whether or not the user requested to pause while buffering.  If set true, playback will not resume
     * playing once buffering has ended.
     */
    private boolean mWasPausedWhenBuffering = false;

    private Profiler mProfiler = new Profiler();

    private AlertDialog mAlertDialog;

    private PlaybackInfoCallback mPlaybackInfoCallback;

    private static int FADEOUT_TIME = 2000;

    public XVideoView(Context context)
    {
        super(context);
        init();
    }

    public XVideoView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    public XVideoView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init();
    }

    private void init()
    {
        final View v = LayoutInflater.from(getContext()).inflate(
                R.layout.x_video_view, this);

        mVideoView = (VideoView) v.findViewById(R.id.video_view);
        mVideoView.setOnPreparedListener(this);
        mVideoView.setOnErrorListener(this);
        mVideoView.setOnCompletionListener(this);
        mVideoView.setOnTouchListener(this);

        mBufferingSpinner = (ProgressBar) v
                .findViewById(R.id.buffer_progress_spinner);

        mMediaControlsPanel = v.findViewById(R.id.media_controls_panel);
        
        //mLogoImage = v.findViewById(R.id.logo_imageview);
        
        mConnectionStatuTextView = (TextView)v.findViewById(R.id.connection_status_textview);

        mScrubberSeekBar = (SeekBar) v.findViewById(R.id.scrubber_seek_bar);
        mScrubberSeekBar.setMax(MAX);

        mDurationTextView = (TextView) v.findViewById(R.id.video_duration);
        mTimeElapsedTextView = (TextView) v.findViewById(R.id.time_elapsed);
        //mDurationTextView.setTextColor(Color.WHITE);
        //mTimeElapsedTextView.setTextColor(Color.WHITE);

        mHomeImageButton = (ImageButton) v.findViewById(R.id.home_button);
        mPausePlayImageButton = (ImageButton) v.findViewById(R.id.pause_play);
        mNextImageButton = (ImageButton) v.findViewById(R.id.next_button);
        
        mNextCategoryTextView = (TextView) v.findViewById(R.id.next_category_textview);
        mNextNameTextView = (TextView) v.findViewById(R.id.next_name_textview);
        
        mHomeImageButton.setOnClickListener(this);
        mPausePlayImageButton.setOnClickListener(this);
        mNextImageButton.setOnClickListener(this);

        if (ALLOW_SEEKING)
        {
            mScrubberSeekBar.setOnSeekBarChangeListener(this);
        } else
        {
            // mScrubberSeekBar.getThumb().setVisible(false, true);
            mScrubberSeekBar.setEnabled(false);
        }

        mHandler = new MyUiHandler();
    }
    
    public void setConnectionStatusString(final String status)
    {
    	mConnectionStatuTextView.setText(status);
    }
    
    public void setPlaybackInfoCallback(final PlaybackInfoCallback playbackErrorCallback)
    {
    	mPlaybackInfoCallback = playbackErrorCallback;
    }

    //public void initializeXVideoView(final String movie, final String haptic, final boolean isStreaming)
    public void initializeXVideoView(final VideoData videoData)
    {
    	final Context ctx = getContext();
    	
    	//mIsInitialized = true;
    	
    	mRequestedBufferPosition = 0;
        mBufferRequestId = 0;
        
        showSpinner();
        
        final VideoData nextVD = VideoDataHelper.getNextVideoData(ctx, videoData);
        
        mNextCategoryTextView.setText(nextVD.getPageTitle());
        mNextNameTextView.setText(nextVD.getVideoTitle());
        
        stopVideoPlayback();
        mVideoView.setVideoURI(Uri.parse(videoData.getVideoUri()));
        
        killHapticPlaybackThread();
        
        mHapticPlaybackThread = new HapticPlaybackThread(ctx, videoData.getHapticUri(), mHandler, false);
        mHapticPlaybackThread.start();
    }
    
    /*
    public boolean isInitialized()
    {
    	return mIsInitialized;
    }
    */
    
    public void hideSpinner()
    {
    	mBufferingSpinner.setVisibility(GONE);
    }
    
    public void showSpinner()
    {
    	mBufferingSpinner.setVisibility(VISIBLE);
    }

    public void onResume()
    {

    }

    public void onPause()
    {
        pause();
    }

    public void onDestroy()
    {
    	killHapticPlaybackThread();
    	stopVideoPlayback();
    }
    
    public void stopVideoPlayback()
    {
    	if(mVideoView != null)
    	{
    		mVideoView.stopPlayback();
    	}
    }
    
    private void killHapticPlaybackThread()
    {
    	if (mHapticPlaybackThread != null)
        {
            mHapticPlaybackThread.quit();
            mHapticPlaybackThread = null;
        }
    }

    /* ------ OnSeekBarChangeListener ------ */

    @Override
    public void onStartTrackingTouch(SeekBar seekBar)
    {
        mIsSeeking = true;

        
        showMediaControlsPanelIndefinitely();

        // guards against rapid taps
        if (mWasPlaying == false)
        {
            mWasPlaying = isPlaying();
        }

        pause();
    }

    @Override
    public void onProgressChanged(final SeekBar seekBar, final int progress,
            final boolean fromUser)
    {
        mSeekToPosition = progress;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar)
    {
        seekTo((int) ((float) mSeekToPosition / (float) MAX * getDuration()));

        if (mWasPlaying)
        {
            mWasPlaying = false;
            start();
        }

        postDelayed(mHideRunnable, FADEOUT_TIME);

        mIsSeeking = false;
    }

    @Override
	public void onClick(View v)
	{
		switch(v.getId())
		{
			case R.id.home_button:
				
				if(mPlaybackInfoCallback != null)
				{
					mPlaybackInfoCallback.onQuitVideoPlayback();
				}
				
				break;
				
			case R.id.pause_play:
				
				pausePlayButtonClick();
				
				break;
				
			case R.id.next_button:
				
				if(mPlaybackInfoCallback != null)
				{
					mPlaybackInfoCallback.onPlayNextVideo();
				}
				
				break;
		}
	}
    
    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
    	
    	if (v.getId() == R.id.video_view && event.getAction() == MotionEvent.ACTION_DOWN)
        {
            if(mMediaControlsPanel.isShown())
            {
            	hideMediaControlsPanel();
            }
            else
            {
            	showMediaControlsPanel();
            }
        }
    	/*
        if (v.getId() == R.id.video_view 
                && event.getAction() == MotionEvent.ACTION_DOWN
                && mScrubberSeekBar.isShown())
        {
            pausePlayButtonClick();
        }
        else 
        {
            mPausePlayImageButton.setImageResource(R.drawable.pause_button);
            showMediaControlsPanel();
        }
		*/
        return false;
    }

    private void pausePlayButtonClick()
    {
        if (mIsPrepared)
        {
            if (isPlaying() || mIsStarting)
            {
                mIsStarting = false;
                pause();
                mPausePlayImageButton.setImageResource(R.drawable.play_button);
                
                showMediaControlsPanelIndefinitely();

                if (mIsBuffering)
                {
                    mWasPausedWhenBuffering = true;
                }
            } else
            {

            	BluetoothCommunication.getInstance().sendResumeFromPauseEvent(0, 0);
                mIsStarting = true;
                start();
                removeCallbacks(mHideRunnable);
                postDelayed(mHideRunnable, FADEOUT_TIME);
                mPausePlayImageButton.setImageResource(R.drawable.pause_button);
            }
        } else
        {
            if (mWasPausedWhenNotPrepared)
            {
                mWasPausedWhenNotPrepared = false;

                removeCallbacks(mHideRunnable);
                postDelayed(mHideRunnable, FADEOUT_TIME);
                mPausePlayImageButton.setImageResource(R.drawable.pause_button);
            } else
            {
                mWasPausedWhenNotPrepared = true;
                mPausePlayImageButton.setImageResource(R.drawable.play_button);
            }
        }
    }

    /**
     * Called when AV and/or haptic buffering has begun.
     */
    private void onBufferingStart()
    {
        mIsBuffering = true;
    }

    /**
     * Called when buffering has ended, and reports back to the caller if the
     * user attempted to pause during the buffering operation.
     * 
     * @return True if the user attempted to pause during a buffering operation,
     *         false otherwise.
     */
    private boolean onBufferingComplete()
    {
        final boolean tmp = mWasPausedWhenBuffering;

        mWasPausedWhenBuffering = false;
        mIsBuffering = false;

        return tmp;
    }

    /**
     * Immediately hide the media control panel.
     */
    public void hideMediaControlsPanel()
    {
        removeCallbacks(mHideRunnable);

        //mLogoImage.setVisibility(GONE);
        mMediaControlsPanel.setVisibility(GONE);
    }

    /**
     * Show the media control panel, and automatically hide it 3 seconds later.
     */
    public void showMediaControlsPanel()
    {
        removeCallbacks(mHideRunnable);

        //mLogoImage.setVisibility(VISIBLE);
        mMediaControlsPanel.setVisibility(VISIBLE);
        
        postDelayed(mHideRunnable, FADEOUT_TIME);
    }

    /**
    * Show the media control panel, but do not automatically hide it.
    */
    public void showMediaControlsPanelIndefinitely()
    {
        removeCallbacks(mHideRunnable);
        
        //mLogoImage.setVisibility(VISIBLE);
        mMediaControlsPanel.setVisibility(VISIBLE);
        
        //mPausePlayImageButton.setImageResource(R.drawable.play_button);
    }

    private Runnable mHideRunnable = new Runnable()
    {

        @Override
        public void run()
        {
            Animation fadeOut = new AlphaAnimation(1, 0);
            fadeOut.setInterpolator(new AccelerateInterpolator());
            fadeOut.setStartOffset(1000);
            fadeOut.setDuration(1000);

            //mLogoImage.setAnimation(fadeOut);
            mMediaControlsPanel.setAnimation(fadeOut);
            
            //mLogoImage.setVisibility(GONE);
            mMediaControlsPanel.setVisibility(GONE);
        }
    };

    public void playbackStarted()
    {
        mIsStarting = false;
        mHandler.post(mPlaybackPositionPoll);
        mHandler.postDelayed(mPeriodicSyncRunnable, 500l);
        String seconds = String.format("%02d", ((getDuration() / 1000) % 60));
        String minute = String.format("%02d", ((getDuration() / (1000 * 60)) % 60));

        mPausePlayImageButton.setImageResource(R.drawable.pause_button);
        mDurationTextView.setText(minute + ":" + seconds);
    }

    private Runnable mPlaybackPositionPoll = new Runnable()
    {
        @Override
        public void run()
        {
            if (isPlaying())
            {
                mScrubberSeekBar
                        .setProgress((int) (((float) getCurrentPosition() / (float) getDuration()) * MAX));

                String time = String.format("%02d:%02d", ((getCurrentPosition() / 60000)),
                        (getCurrentPosition() / (1000)) % 60);
                mTimeElapsedTextView.setText(time);

                mHandler.postDelayed(this, 100l);
                //mHandler.postDelayed(this, 5l);
                
                //Log.d(TAG, "getCurrentPosition() = " + getCurrentPosition());
            }
        }
    };

    /**
     * This runnable is posted the ui handler queue and allows the rewind and fast forward buttons to work. 
     */
    private Runnable mSeekToAndStartRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            mScrubberSeekBar
                    .setProgress((int) (((float) mRewindFastForwardPosition / (float) getDuration()) * MAX));
            Log.d(TAG,
                    "--mSeekToAndStartRunnable-- mRewindFastForwardPosition = "
                            + mRewindFastForwardPosition);
            seekTo(mRewindFastForwardPosition);
            start();
        }
    };

    @Override
    public void onPrepared(MediaPlayer mp)
    {
        Log.d(TAG, "-------- onPrepared --------");

        mIsPrepared = true;

        mp.setOnBufferingUpdateListener(this);
        mp.setOnSeekCompleteListener(this);
        mp.setOnInfoListener(this);

        /*
         * if(mVideoView.canSeekForward()) { Log.d(TAG, "can seek forward"); }
         * 
         * if(mVideoView.canSeekBackward()) { Log.d(TAG, "can seek backward"); }
         */

        mRewindFastForwardDelta = (int) (getDuration() * 0.1f);
        // Make the minimal delta one second
        mRewindFastForwardDelta = mRewindFastForwardDelta < 1000 ? 1000
                : mRewindFastForwardDelta;

        if (!mWasPausedWhenNotPrepared)
        {
            mIsStarting = true;
            start();
            showMediaControlsPanel();
        }
    }

    @Override
    public boolean canPause()
    {
        return mVideoView.canPause();
    }

    @Override
    public boolean canSeekBackward()
    {
        return mVideoView.canSeekBackward();
    }

    @Override
    public boolean canSeekForward()
    {
        return mVideoView.canSeekForward();
    }

    @Override
    public int getBufferPercentage()
    {
        return mVideoView.getBufferPercentage();
    }

    @Override
    public int getCurrentPosition()
    {
        // NOTE: getCurrentPosition() has a resolution of about 42 ms... This may be due
        // to most video having a frame rate of 24 fps --> since 1000 / 24 = 41.67

        // Log.d(TAG, "-------- getCurrentPosition() --------");
        return mVideoView.getCurrentPosition();
    }

    @Override
    public int getDuration()
    {
        // Log.d(TAG, "-------- getDuration() --------");
        return mVideoView.getDuration();
    }

    @Override
    public boolean isPlaying()
    {
        // Log.d(TAG, "-------- isPlaying() --------");
        return mVideoView.isPlaying();
    }

    @Override
    public void pause()
    {
        Log.d(TAG, "-------- onPause() --------");
        
        BluetoothCommunication.getInstance().sendPauseEvent();
        
        mVideoView.pause();
        mRequestedBufferPosition = getCurrentPosition();
        stopHapticPlayback();
        showMediaControlsPanelIndefinitely();
    }

    @Override
    public void seekTo(int pos)
    {
        Log.d(TAG, "-------- seekTo(" + pos + ") ----------");

        //mRequestedBufferPosition = pos > 0 ? (pos / 250) * 250 : 0;
        //mRequestedBufferPosition = pos > 0 ? (pos / 500) * 500 : 0;
        mRequestedBufferPosition = pos > 0 ? (pos / 1000) * 1000 : 0;

        Log.d(TAG, "mRequestedBufferPosition = " + mRequestedBufferPosition);

        mVideoView.seekTo(mRequestedBufferPosition);

        BluetoothCommunication.getInstance().sendResumeFromPauseEvent(0, 0);

        Log.d(TAG, "media player's position after seekTo = "
                + getCurrentPosition());
    }

    @Override
    public void start()
    {
        Log.d(TAG, "-------- start() --------");

        prepareHapticPlayback();
    }

    /**
     * Tell the HapticPlaybackThread to buffer up haptic data for playback at
     * the current playback position of the MediaPlayer.
     */
    private void prepareHapticPlayback()
    {
        Log.d(TAG, "@@@@@@@@@@@@@@@@@@ prepareHapticPlayback("
                + getCurrentPosition() + ") @@@@@@@@@@@@@@@@@@");
        mProfiler.startTiming();

        onBufferingStart();
        showSpinner();

        ++mBufferRequestId;
        mHapticPlaybackThread.prepareHapticPlayback(mRequestedBufferPosition,
                mBufferRequestId);
    }

    private void playHapticForCurrentPlaybackPosition(final int playbackPosition, final long uptimeMS)
    {
        //Log.d(TAG, "************** playHapticForCurrentPlaybackPosition(" + getCurrentPosition() + ") ******************");
        //Log.d(TAG, "************** playHapticForCurrentPlaybackPosition(" + playbackPosition + ") ******************");
        
        /*
         * final int test = getCurrentPosition();
         * 
         * if(playbackPosition != test) { Log.e(TAG,
         * "playbackPosition != test"); Log.e(TAG, "test - playbackPosition = "
         * + (test - playbackPosition));
         * 
         * throw new RuntimeException("playbackPosition != test"); }
         */

        hideSpinner();

        //mHapticPlaybackThread.playHapticForPlaybackPosition(playbackPosition, SystemClock.uptimeMillis());
        mHapticPlaybackThread.playHapticForPlaybackPosition(playbackPosition, uptimeMS);
    }

    private void stopHapticPlayback()
    {
        Log.d(TAG, "-------------- stopHapticPlayback() ---------------");
        mHapticPlaybackThread.stopHapticPlayback();
    }

    @Override
    public void onCompletion(MediaPlayer mp)
    {
        mHapticPlaybackThread.stopHapticPlayback();
        
        //final BluetoothCommunication btc = ((BaseApplication)(getContext().getApplicationContext())).mBluetoothCommunication;
        
        // By calling startAcceptThread we destroy the current BluetoothSocket and begin looking for a reconnection.  This will
        // signal the watch to stop it's connection thread and re-establish a new connection with the phone.
        //btc.startAcceptThread();
        
        if(mPlaybackInfoCallback != null)
        {
            mPlaybackInfoCallback.onPlaybackComplete();
        }
    }

    @Override
    public void onSeekComplete(MediaPlayer mp)
    {
        Log.d(TAG, "onSeekComplete() current player position = " + mp.getCurrentPosition());
        mScrubberSeekBar.setProgress((int) (((float) getCurrentPosition() / (float) getDuration()) * MAX));
        
        /*
        if(mPlaybackSeek)
        {
            mVideoView.seekTo(mRequestedBufferPosition);
            mPlaybackSeek = false;
        }
        */
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra)
    {
        Log.d(TAG, "-------- onError() --------");

        switch (what)
        {
            case MEDIA_ERROR_SERVER_DIED:

                handleOnErrorExtra("MEDIA_ERROR_SERVER_DIED", extra);

                Log.d(TAG, "MEDIA_ERROR_SERVER_DIED = "
                        + MEDIA_ERROR_SERVER_DIED);

                break;

            case MEDIA_ERROR_UNKNOWN:

                handleOnErrorExtra("MEDIA_ERROR_UNKNOWN", extra);

                Log.d(TAG, "MEDIA_ERROR_UNKNOWN = " + MEDIA_ERROR_UNKNOWN);

                break;
        }

        Log.d(TAG, "onError() extra = " + extra);

        return true;
    }

    private void handleOnErrorExtra(final String whatMessage, final int extra)
    {
        final StringBuilder sb = new StringBuilder(whatMessage);
        sb.append(" : extra info = ");

        switch (extra)
        {
            case MEDIA_ERROR_IO:

                sb.append("The file you are trying to access may be corrupted.");

                Log.d(TAG, "MEDIA_ERROR_IO = " + MEDIA_ERROR_IO);

                break;

            case MEDIA_ERROR_MALFORMED:

                sb.append("MEDIA_ERROR_MALFORMED");

                Log.d(TAG, "MEDIA_ERROR_MALFORMED = " + MEDIA_ERROR_MALFORMED);

                break;

            case MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:

                sb.append("MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK");

                Log.d(TAG, "MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK = "
                        + MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK);

                break;

            case MEDIA_ERROR_UNSUPPORTED:

                sb.append("MEDIA_ERROR_UNSUPPORTED");

                Log.d(TAG, "MEDIA_ERROR_UNSUPPORTED = "
                        + MEDIA_ERROR_UNSUPPORTED);

                break;

            case MEDIA_ERROR_TIMED_OUT:

                sb.append("MEDIA_ERROR_TIMED_OUT");

                Log.d(TAG, "MEDIA_ERROR_TIMED_OUT = " + MEDIA_ERROR_TIMED_OUT);

                break;
        }

        showAlertDialog(sb.toString(), false);
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra)
    {
        Log.d(TAG, "-------- onInfo() beginning --------");
        Log.d(TAG, "what = " + what);

        switch (what)
        {
            case MEDIA_INFO_BAD_INTERLEAVING:

                Log.d(TAG, "MEDIA_INFO_BAD_INTERLEAVING = "
                        + MEDIA_INFO_BAD_INTERLEAVING);

                break;

            case MEDIA_INFO_BUFFERING_END:

                Log.d(TAG, "MEDIA_INFO_BUFFERING_END = "
                        + MEDIA_INFO_BUFFERING_END + " playback position = "
                        + getCurrentPosition());

                mVideoView.pause();
                
                // Ugh, this is ugly! ... Calling seekTo() here is an attempt to reposition everything to the beginning of an AV-frame.
                seekTo(getCurrentPosition());
                //mRequestedBufferPosition = getCurrentPosition();
                
                if(!onBufferingComplete())
                {
                    prepareHapticPlayback();
                }

                hideSpinner();

                break;

            case MEDIA_INFO_BUFFERING_START:
                
                // !!! Important !!! -- Here we don't call mVideoView.pause(), because it halts buffering
                onBufferingStart();
                showSpinner();
                stopHapticPlayback();

                Log.d(TAG, "MEDIA_INFO_BUFFERING_START = "
                        + MEDIA_INFO_BUFFERING_START + " playback position = "
                        + getCurrentPosition());

                break;

            case MEDIA_INFO_METADATA_UPDATE:

                Log.d(TAG, "MEDIA_INFO_METADATA_UPDATE = "
                        + MEDIA_INFO_METADATA_UPDATE);

                break;

            case MEDIA_INFO_NOT_SEEKABLE:

                Log.d(TAG, "MEDIA_INFO_NOT_SEEKABLE = "
                        + MEDIA_INFO_NOT_SEEKABLE);

                break;

            case MEDIA_INFO_UNKNOWN:

                Log.d(TAG, "MEDIA_INFO_UNKNOWN = " + MEDIA_INFO_UNKNOWN);

                break;

            case MEDIA_INFO_VIDEO_RENDERING_START:

                Log.d(TAG, "MEDIA_INFO_VIDEO_RENDERING_START = "
                        + MEDIA_INFO_VIDEO_RENDERING_START);

                break;

            case MEDIA_INFO_VIDEO_TRACK_LAGGING:

                Log.d(TAG, "MEDIA_INFO_VIDEO_TRACK_LAGGING = "
                        + MEDIA_INFO_VIDEO_TRACK_LAGGING);

                break;
        }

        Log.d(TAG, "extra = " + extra);

        Log.d(TAG, "-------- onInfo() end --------");

        return false;
    }

    private boolean mPlaybackSeek;
    private class MyUiHandler extends Handler
    {
        @Override
        public void handleMessage(final Message msg)
        {
            Log.d(TAG,
                    "prepare haptic round trip duration = "
                            + mProfiler.getDuration());

            switch (msg.what)
            {
                case HAPTIC_PLAYBACK_IS_READY:

                    Log.d(TAG,
                            "&&&&&&&&&&&&&&&&&&&&&&&&&&&& case HAPTIC_PLAYBACK_IS_READY");
                    Log.d(TAG, "handleMessage() current position = "
                            + mVideoView.getCurrentPosition());

                    if (!onBufferingComplete() && !mIsSeeking
                            && msg.arg1 == mRequestedBufferPosition
                            && msg.arg2 == mBufferRequestId)
                    {
                        //mPlaybackSeek = true;
                        
                        
                        //Log.d(TAG, "handleMessage() is handling position = " + mRequestedBufferPosition + " and requestID = " + mBufferRequestId);
                        
                        //mProfiler.startTimingII();
                        
                        //final long uptimeMS = SystemClock.uptimeMillis();
                        mVideoView.start();

                        // hackish!!!
                        // mVideoView.seekTo(mRequestedBufferPosition);

                        //Log.d(TAG, "mVideoView.start() duration = " + mProfiler.getDurationII());

                        playHapticForCurrentPlaybackPosition(mRequestedBufferPosition, SystemClock.uptimeMillis());
                        //playHapticForCurrentPlaybackPosition(mRequestedBufferPosition, uptimeMS);

                        Log.d(TAG,
                                "playHapticForCurrentPlaybackPosition() duration = "
                                        + mProfiler.getDurationII());

                        playbackStarted();
                    }

                    break;

                case PAUSE_AV_FOR_HAPTIC_BUFFERING:

                    mVideoView.pause();
                    seekTo(msg.arg1);
                    prepareHapticPlayback();

                    break;

                case HAPTIC_DOWNLOAD_ERROR:

                    handleHapticDownloadError(msg);

                    break;

                case HAPTIC_BUFFER_UP_TIMEOUT:

                    handleBufferUpTimeout();

                    break;
                    
                case AV_SEEK_TO:
                    
                    Log.d(TAG, "correction = " + msg.arg1);
                    mVideoView.seekTo(msg.arg1);
                    
                    break;
            }
        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent)
    {
        // Log.d(TAG, ">>>>>>>>>>>>>>> onBufferingUpdate percent done = " +
        // percent + "%");
        // Log.d(TAG, "getBufferPercentage() = " + getBufferPercentage());

        mScrubberSeekBar.setSecondaryProgress((int) (percent / 100.0f * MAX));
    }

    private void showAlertDialog(final String message, final boolean retryDialog)
    {
        hideAlertDialog();

        final AlertDialog.Builder b = new AlertDialog.Builder(getContext());

        b.setMessage(message);

        if (retryDialog)
        {
            b.setNegativeButton(R.string.quit, mDialogOnClickListener);
            b.setPositiveButton(R.string.retry, mDialogOnClickListener);
        } else
        {
            b.setNegativeButton(R.string.ok, mDialogOnClickListener);
        }

        mAlertDialog = b.show();
    }

    private void hideAlertDialog()
    {
        if (mAlertDialog != null)
        {
            mAlertDialog.dismiss();
        }
    }

    private void handleHapticDownloadError(final Message msg)
    {
        final Bundle b = msg.getData();

        final Exception e = (Exception) b
                .getSerializable(HAPTIC_DOWNLOAD_EXCEPTION_KEY);

        if (e instanceof HttpUnsuccessfulException)
        {
            final HttpUnsuccessfulException hue = (HttpUnsuccessfulException) e;
            Log.d(TAG, "caught HttpUnsuccessfulExcetion http status code = "
                    + hue.getHttpStatusCode());
        }

        showAlertDialog(e.getMessage(), false);
    }

    public void handleBufferUpTimeout()
    {
        showAlertDialog(getContext().getString(R.string.buffer_up_timeout_msg),
                true);
    }

    private DialogInterface.OnClickListener mDialogOnClickListener = new DialogInterface.OnClickListener()
    {

        @Override
        public void onClick(DialogInterface dialog, int which)
        {
            switch (which)
            {
                case AlertDialog.BUTTON_NEGATIVE:
                    
                    if(mPlaybackInfoCallback != null)
                    {
                        mPlaybackInfoCallback.onPlaybackError();
                    }

                    break;

                case AlertDialog.BUTTON_POSITIVE:

                    prepareHapticPlayback();

                    break;
            }
        }
    };

    
    /**
     * A callback that allows the {@link XVideoView} to inform the implementor when an AV-Haptic playback error occurs.
     * The implementor is usually the {@link Activity} hosting video playback.
     * 
     * @author ivan stashak
     *
     */
    public interface PlaybackInfoCallback
    {
        public void onPlaybackComplete();
        public void onPlaybackError();
        public void onQuitVideoPlayback();
        
        public void onPlayPreviousVideo();
        public void onPlayNextVideo();
    }

    private Runnable mPeriodicSyncRunnable = new Runnable()
    {

        @Override
        public void run()
        {
            int pos = getCurrentPosition();
            if (isPlaying())
            {
            	BluetoothCommunication.getInstance().sendTimeStamp(pos);

                mHapticPlaybackThread.syncUpdate(pos, SystemClock.uptimeMillis(), ++mBufferRequestId);

                mHandler.postDelayed(this, 1000l);
            }
        }

    };

	@Override
	public int getAudioSessionId()
	{
		return mVideoView.getAudioSessionId();
	}

}

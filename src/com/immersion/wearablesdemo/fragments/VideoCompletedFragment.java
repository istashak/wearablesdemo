package com.immersion.wearablesdemo.fragments;


import com.immersion.wearablesdemo.R;
import com.immersion.wearablesdemo.activities.ScreenSlideActivity;
import com.immersion.wearablesdemo.activities.VideoActivity;
import com.immersion.wearablesdemo.models.VideoData;
import com.immersion.wearablesdemo.utils.VideoDataHelper;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * @author ivan stashak
 *
 */
public class VideoCompletedFragment extends Fragment implements OnClickListener
{

	private VideoData mPreviousVideoData;
	private VideoData mVideoData;
	private VideoData mNextVideoData;
	
	private ImageView mLogoImageView;
	
	private ImageButton mPreviousImageButton;
	private ImageButton mPlayAgainImageButton;
	private ImageButton mNextImageButton;
	private ImageButton mHomeImageButton;
	
	private View mPreviousLayout;
	private ImageView mPreviousButtonBGImageView;
	private TextView mPreviousCategoryTextView;
	private TextView mPreviousNameTextView;
	
	private View mNextLayout;
	private ImageView mNextButtonBGImageView;
	private TextView mNextCategoryTextView;
	private TextView mNextNameTextView;
	
	private boolean mIsFirstStart = true;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		final View view = inflater.inflate(R.layout.video_complete_fragment, container, false);
		
		mLogoImageView = (ImageView)view.findViewById(R.id.immersion_logo_imageview);
		
		mPreviousLayout = view.findViewById(R.id.previous_layout);
		mPreviousButtonBGImageView = (ImageView)mPreviousLayout.findViewById(R.id.previous_button_background_imageview);
		mPreviousCategoryTextView = (TextView)mPreviousLayout.findViewById(R.id.previous_category_textview);
		mPreviousNameTextView = (TextView)mPreviousLayout.findViewById(R.id.previous_name_textview);
		
		mNextLayout = view.findViewById(R.id.next_layout);
		mNextButtonBGImageView = (ImageView)mNextLayout.findViewById(R.id.next_button_background_imageview);
		mNextCategoryTextView = (TextView)mNextLayout.findViewById(R.id.next_category_textview);
		mNextNameTextView = (TextView)mNextLayout.findViewById(R.id.next_name_textview);
		
		mPreviousImageButton = (ImageButton)view.findViewById(R.id.previous_imagebutton);
		mPreviousImageButton.setOnClickListener(this);
		
		mPlayAgainImageButton = (ImageButton)view.findViewById(R.id.play_again_imagebutton);
		mPlayAgainImageButton.setOnClickListener(this);
		
		mNextImageButton = (ImageButton)view.findViewById(R.id.next_imagebutton);
		mNextImageButton.setOnClickListener(this);
		
		mHomeImageButton = (ImageButton)view.findViewById(R.id.home_imagebutton);
		mHomeImageButton.setOnClickListener(this);
		
		return view;
	}

	@Override
	public void onStart()
	{
		super.onStart();
		
		if(mIsFirstStart)
		{
			mPreviousButtonBGImageView.setImageBitmap(mPreviousVideoData.getMovieImage());
			mPreviousCategoryTextView.setText(mPreviousVideoData.getPageTitle());
			mPreviousNameTextView.setText(mPreviousVideoData.getVideoTitle());
			
			mNextButtonBGImageView.setImageBitmap(mNextVideoData.getMovieImage());
			mNextCategoryTextView.setText(mNextVideoData.getPageTitle());
			mNextNameTextView.setText(mNextVideoData.getVideoTitle());
			
			
			//mLogoImageView.setAlpha(0.0f);
			setFadeInAnimation(mLogoImageView, 750l, 500l);
			setFadeInAnimation(mPlayAgainImageButton, 750l, 500l);
			
			setFadeInAnimation(mHomeImageButton, 750l, 750l);
			
			setFadeInAnimation(mPreviousLayout, 750l, 1000l);
			setFadeInAnimation(mNextLayout, 750l, 1000l);
			
			
			mIsFirstStart = false;
		}
	}
	
	/**
	 * This function was added as a convenience method for passing {@link VideoData} down to the fragment to avoid using the 
	 * setArguments() function, which requires creating a {@link Bundle} and adding a key/value pair.
	 * 
	 * @param videoData
	 */
	public void setVideoData(final VideoData videoData)
	{
		mVideoData = videoData;
	}
	
	@Override
	public void onAttach(final Activity activity)
	{
		super.onAttach(activity);
	
		if(mVideoData != null)
		{	
			mPreviousVideoData = VideoDataHelper.getPreviousVideoData(activity, mVideoData);
			mNextVideoData = VideoDataHelper.getPreviousVideoData(activity, mVideoData);
		}
	}
	
	@Override
	public void onClick(View v)
	{
		switch(v.getId())
		{
			case R.id.previous_imagebutton:
				handlePreviousClick();
				break;
				
			case R.id.play_again_imagebutton:
				handlePlayAgainClick();
				break;
				
			case R.id.next_imagebutton:
				handleNextClick();
				break;
				
			case R.id.home_imagebutton:
				handleHomeClick();
				break;
		}
	}
	
	private void setVideoDataAndPopStack(final VideoData videoData)
	{
		final VideoPlayerFragment videoFrag = (VideoPlayerFragment)getTargetFragment();
		videoFrag.setVideoData(videoData);
		
		getActivity().getFragmentManager().popBackStack();
	}
	
	private void handlePreviousClick()
	{
		setVideoDataAndPopStack(mPreviousVideoData);
	}
	
	private void handlePlayAgainClick()
	{
		getActivity().getFragmentManager().popBackStack();
	}
	
	private void handleNextClick()
	{
		setVideoDataAndPopStack(mNextVideoData);
	}
	
	private void handleHomeClick()
	{
		getActivity().finish();
	}
	
	/**
	 * 
	 * @param obj -- An {@link Object} to fade in, usually a {@link View}.
	 * @param duration -- The duration of the fade-in in milliseconds.
	 * @param startDelay -- The amount of milliseconds to wait before starting the fade-in.
	 */
	private void setFadeInAnimation(final Object obj, final long duration, final long startDelay)
	{
		final ObjectAnimator objAnim = ObjectAnimator.ofFloat(obj, "alpha", 0.0f, 1.0f);
		objAnim.setDuration(duration);
		objAnim.setStartDelay(startDelay);
		objAnim.start();
	}
	
}

package com.immersion.wearablesdemo.fragments;

import java.util.List;

import com.immersion.wearablesdemo.R;
import com.immersion.wearablesdemo.activities.ScreenSlideActivity;
import com.immersion.wearablesdemo.activities.VideoActivity;
import com.immersion.wearablesdemo.models.VideoData;
import com.immersion.wearablesdemo.utils.LetterSpacingTextView;
import com.immersion.wearablesdemo.utils.VideoDataHelper;

import android.app.Activity;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ScreenSlidePageFragment extends Fragment implements OnTouchListener
{
	 public static final String ARG_PAGE = "page";
	    private int mPageNumber;
	    private ImageView play[] = new ImageView[3];
	    private ImageView vignette[] = new ImageView[3];
		private VideoData videoInfo;
		private ViewGroup rootView;
		private int moveCount = 0;
		
		private List<VideoData> mVideoData;
		
	    public static ScreenSlidePageFragment create(int pageNumber) {
	        ScreenSlidePageFragment fragment = new ScreenSlidePageFragment();
	        Bundle args = new Bundle();
	        args.putInt(ARG_PAGE, pageNumber);
	        fragment.setArguments(args);
	        return fragment;
	    }
	    
	    @Override
	    public void onResume(){
	    	super.onResume();
	    	VideoData vd;


	    }
	 
	    @Override
	    public void setArguments(final Bundle bundle)
	    {
	    	super.setArguments(bundle);
	    	mPageNumber = bundle.getInt(ARG_PAGE);
	    	
	    }
	    
	    @Override
	    public void onAttach(final Activity activity)
	    {
	    	super.onAttach(activity);
	    	
	    	mVideoData = VideoDataHelper.getPageVideoData(activity, mPageNumber);
	    }

	    @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	    {
	    	VideoData vd = mVideoData.get(0);
	    	final Activity act = getActivity();
    		rootView = (ViewGroup) inflater.inflate(R.layout.fragment_screen_slide_page, container, false);
    		LetterSpacingTextView title = (LetterSpacingTextView) rootView.findViewById(R.id.text1);
    		title.setText(vd.getPageTitle());
    		Typeface lightFont = Typeface.createFromAsset(act.getAssets(), "opensans_light.ttf");
    		title.setTypeface(lightFont);
    		Typeface regFont = Typeface.createFromAsset(act.getAssets(), "opensans_regular.ttf");
    		title.setTypeface(lightFont);
    		title.setLetterSpacing(-2.5f);
    		
    		TextView desc = (TextView) rootView.findViewById(R.id.text2);
    		desc.setText(vd.getPageDescription());
    		desc.setTypeface(regFont);
	        
    		int numberOfMovies = mVideoData.size();
	        for (int i = 0; i < numberOfMovies;i++){
	    		vd = mVideoData.get(i);
	            LinearLayout movieHolder = (LinearLayout) rootView.findViewById(R.id.movieHolder);
	            
	            RelativeLayout relativeLayout = new RelativeLayout(act);
	            RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
	                    RelativeLayout.LayoutParams.WRAP_CONTENT,
	                    RelativeLayout.LayoutParams.WRAP_CONTENT);
	            movieHolder.addView(relativeLayout);

	            vignette[i] = new ImageView(act);
	            vignette[i].setVisibility(View.VISIBLE);
	            vignette[i].setImageBitmap(vd.getMovieImage());
	            vignette[i].setLongClickable(true);
	            vignette[i].setOnTouchListener(this);
	            vignette[i].setTag(vignette[i].getId(), vd);
	            relativeLayout.addView(vignette[i]);

	            play[i] = new ImageView(act);
	            play[i].setVisibility(View.VISIBLE);
	            play[i].setImageResource(R.drawable.play_grey);
	            RelativeLayout.LayoutParams layoutParams=new RelativeLayout.LayoutParams(150, 150);
	            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
	            play[i].setLayoutParams(layoutParams);
	            
	            relativeLayout.addView(play[i]);
	        }
	        
	        return rootView;
	    }

	    public int getPageNumber() {
	        return mPageNumber;
	    }
	    
	    public void startVideoPlayer(final VideoData videoData){
	    	final Activity act = getActivity();
	    	final Intent i = new Intent(act, VideoActivity.class);
	    	i.putExtra(VideoActivity.VIDEO_DATA_EXTRA, videoData);
	    	act.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
	    	act.startActivity(i);
	    }

		@Override
		public boolean onTouch(View v, MotionEvent event)
		{
			int wantedMovie = -1;
	    	final ScreenSlideActivity act = (ScreenSlideActivity)getActivity();
	    	
	    	if (act.infoViewIsVisible == true){
	    		return false;
	    	}
			
			for (int i = 0;i < 3;i++){
				if (v == play[i] || v == vignette[i]){
					wantedMovie = i;
				}
			}
			Log.d ("debug touch", "touch "+ event.getAction());
			
			if (wantedMovie == -1){
				return false;
			}
			
			if (event.getAction() == event.ACTION_UP){
				play[wantedMovie].setImageResource(R.drawable.play_grey);
				final VideoData videoData = (VideoData)v.getTag(v.getId());
	        	startVideoPlayer(videoData);
			} else if (event.getAction() == event.ACTION_DOWN){
				play[wantedMovie].setImageResource(R.drawable.play_orange);
				moveCount = 0;
			} else if (event.getAction() == event.ACTION_MOVE){
				moveCount++;
				if (moveCount == 15){
					play[wantedMovie].setImageResource(R.drawable.play_grey);
				}
				Log.d("debug touch", "move");
			} else {
				play[wantedMovie].setImageResource(R.drawable.play_grey);
			}
			
            return false;
		}
}

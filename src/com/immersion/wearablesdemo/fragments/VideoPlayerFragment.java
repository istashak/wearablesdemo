package com.immersion.wearablesdemo.fragments;

import java.io.File;

import com.immersion.wearablesdemo.R;
import com.immersion.wearablesdemo.controllers.BaseApplication;
import com.immersion.wearablesdemo.controllers.BluetoothCommunication;
import com.immersion.wearablesdemo.controllers.BluetoothCommunication.BluetoothUICallback;
import com.immersion.wearablesdemo.models.VideoData;
import com.immersion.wearablesdemo.utils.VideoDataHelper;
import com.immersion.wearablesdemo.views.NonHapticVideoPlayer;
import com.immersion.wearablesdemo.views.NonHapticVideoPlayer.PlaybackInfoCallback;

import android.app.Activity;
import android.app.Fragment;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.SlidingDrawer;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * @author ivan stashak
 *
 */
public class VideoPlayerFragment extends Fragment implements PlaybackInfoCallback, BluetoothUICallback
{
	private static final String TAG = "VideoPlayerFragment";
	
	private static final int MAX_FILE_UPLOAD_ATTEMPTS = 3;
	
	private int mFileUploadAttempts = 0;
	
	private VideoData mVideoData;
	//private XVideoView mVideoView;
	private NonHapticVideoPlayer mVideoView;
	
	
	private volatile boolean mHapticUploadSuccess = false;
	
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		final View v = inflater.inflate(R.layout.video_player_fragment, container, false);
		
		//mVideoView = (XVideoView) v.findViewById(R.id.x_video_view);
		mVideoView = (NonHapticVideoPlayer) v.findViewById(R.id.my_video_player);
		mVideoView.setPlaybackInfoCallback(this);
		
		return v;
	}
	
	/**
	 * This function was added as a convenience method for passing {@link VideoData} down to the fragment to avoid using the 
	 * setArguments() function, which requires creating a {@link Bundle} and adding a key/value pair.
	 * 
	 * @param videoData
	 */
	public void setVideoData(final VideoData videoData)
	{
		mVideoData = videoData;
	}
	
	@Override
	public void onAttach(final Activity activity)
	{
		super.onAttach(activity);
		
		BluetoothCommunication.getInstance().setBluetoothUICallback(this);
	}
	
	
	
	@Override
	public void onResume()
	{
		super.onResume();
		
		acquireBluetoothAndStartVideo();
	}
	

	/**
	 * This function should only be called once per-video.
	 */
	private void acquireBluetoothAndStartVideo()
	{
		mVideoView.showSpinner();
		
		mHapticUploadSuccess = false;
		
		final BluetoothCommunication btc = BluetoothCommunication.getInstance();
		
		/*
		if(!btc.isConnectedOrConnecting())
		{
			btc.startAcceptThread();
		}
		*/
		
		
		final BluetoothDevice bluetoothDevice = btc.getConnectedBluetoothDevice();
		
		if(bluetoothDevice != null)
		{
			setConnectionStatus("Connected to Gear [" + bluetoothDevice.getName() + "]");
			new HapticUploadThread().start();
		}
		else// if(!btc.isConnecting())
		{
			btc.startAcceptThread();
		}
		
		/*
		else
		{
			//setConnectionStatus("Connecting to Gear");
			setConnectionStatus("Waiting for Gear to Connect");
		}
		*/
	}
		
	/**
	 * Should only be called if mBluetoothCommunication.isConnected() == true.
	 */
	private void initializeVideoView()
	{
		if(mVideoView != null && mVideoData != null)
		{
			//BluetoothCommunication.getInstance().startHapticPlayback();
			mVideoView.initializeXVideoView(mVideoData);
		}
		else
		{
			// TODO: Alert user that video wasn't acquired.
		}
	}
	
	@Override
	public void onDetach()
	{
		super.onDetach();
		
		BluetoothCommunication.getInstance().removeBluetoothUICallback(this);
		//mBluetoothCommunication = null;
	}
	
	
	
	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		
		if(mVideoView != null)
		{
			mVideoView.onDestroy();
			mVideoView = null;
		}
	}
	

	@Override
	public void onPlaybackComplete()
	{
		setHapticFileUploadStatus("");
		BluetoothCommunication.getInstance().onPlaybackComplete();
		openVideoCompleteFragment();
	}


	@Override
	public void onPlaybackError()
	{
		final Activity act = getActivity();
		act.finish();	
	}
	
	
	@Override
	public void onQuitVideoPlayback()
	{
		getActivity().finish();
	}
	
	
	@Override
	public void onPlayPreviousVideo()
	{
		mVideoView.stopVideoPlayback();
		
		mVideoData = VideoDataHelper.getPreviousVideoData(getActivity(), mVideoData);
		
		acquireBluetoothAndStartVideo();
	}

	
	@Override
	public void onPlayNextVideo()
	{
		mVideoView.stopVideoPlayback();
		
		BluetoothCommunication.getInstance().onPlaybackComplete();
		
		mVideoData = VideoDataHelper.getNextVideoData(getActivity(), mVideoData);
		
		acquireBluetoothAndStartVideo();
	}
	
	
	private void openVideoCompleteFragment()
	{
		final Activity act = getActivity();
		
		final String tag = VideoCompletedFragment.class.getName();
		final VideoCompletedFragment fragment = new VideoCompletedFragment();
		
		fragment.setVideoData(mVideoData);
		fragment.setTargetFragment(this, 1);
		
		act.getFragmentManager().beginTransaction().replace(R.id.container, fragment, tag).addToBackStack(tag).commit();
	}

	
	private void setHapticFileUploadStatus(final String status)
	{
		if(mVideoView != null)
		{
			mVideoView.setHapticFileUploadStatus(status);
		}
	}
	
	private void setConnectionStatus(final String status)
	{
		if(mVideoView != null)
		{
			mVideoView.setConnectionStatusString(status);
		}
	}
	

	@Override
	public void onConnected(final BluetoothDevice bluetoothDevice)
	{
		setConnectionStatus("Connected to Gear [" + bluetoothDevice.getName() + "]");
		
		// If we are just getting connected and haven't yet uploaded the haptic file, then do it.
		if(!mHapticUploadSuccess)
		{	
			new HapticUploadThread().start();
		}
	}

	@Override
	public void onHapticFileUploadSuccess()
	{
		setHapticFileUploadStatus("Haptic File Upload Success");
		
		mHapticUploadSuccess = true;
		
		mFileUploadAttempts = 0;
		
		initializeVideoView();
	}

	@Override
	public void onHapticFileUploadFailure()
	{
		final BluetoothCommunication btc = BluetoothCommunication.getInstance();
		
		setHapticFileUploadStatus("Failed to Upload Haptic File");
		
		++mFileUploadAttempts;
		
		if(mFileUploadAttempts > MAX_FILE_UPLOAD_ATTEMPTS)
		{
			final Activity act = getActivity();
			
			Toast.makeText(act, "Failed to upload haptic file. Make sure the watch is near the phone.", Toast.LENGTH_LONG).show();
			act.finish();
		}
		else
		{
			btc.startAcceptThread();
		}
		/*
		else if(btc.isConnected())
		{
			new HapticUploadThread().start();
		}
		else// if(!btc.isConnecting())
		{
			btc.startAcceptThread();
		}
		*/
	}

	@Override
	public void onConnecting()
	{
		//setConnectionStatus("Connecting to Gear");
		setConnectionStatus("Waiting for Gear to Connect");
	}

	@Override
	public void onConnectionFailure()
	{
		Log.d(TAG, "onConnectionFailure()");
		setConnectionStatus("Failed to Connect to Gear");
		
		final BluetoothCommunication btc = BluetoothCommunication.getInstance();
		
		if(!btc.isConnectedOrConnecting())
		{
			btc.startAcceptThread();
		}
	}

	@Override
	public void onDisconnected(final BluetoothDevice bluetoothDevice)
	{
		setConnectionStatus("Connection Dropped from Gear [" + (bluetoothDevice != null ? bluetoothDevice.getName() : "No Device Name Available") + "]");
	}
	
	private class HapticUploadThread extends Thread
	{
		public HapticUploadThread()
		{
			super("HapticUploadThread");
			setHapticFileUploadStatus("Uploading Haptic File...");
			Log.d(TAG, "Uploading haptics for " + mVideoData.getVideoUri());
		}
		
		@Override
		public void run()
		{
			BluetoothCommunication btc = BluetoothCommunication.getInstance();
			
			if(btc != null)
			{
				btc.sendHaptFile(new File(mVideoData.getHapticUri()));
			}
		}
	}
}

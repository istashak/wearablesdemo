package com.immersion.wearablesdemo.activities;

import java.util.ArrayList;
import java.util.List;

import com.immersion.wearablesdemo.R;
import com.immersion.wearablesdemo.controllers.BaseApplication;
import com.immersion.wearablesdemo.controllers.BluetoothCommunication;
import com.immersion.wearablesdemo.fragments.VideoPlayerFragment;
import com.immersion.wearablesdemo.models.VideoData;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

/**
 * 
 * @author ivan stashak
 *
 */
public class VideoActivity extends Activity
{

	private static final String TAG = "VideoActivity";
	
	public static final String VIDEO_DATA_EXTRA = "com.immersion.wearablesdemo.video_data";
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.video_activity);
		
		Log.d(TAG, "onCreate()");
		
		if (savedInstanceState == null)
		{
			final VideoData vd = (VideoData)getIntent().getParcelableExtra(VIDEO_DATA_EXTRA);
			
			final VideoPlayerFragment fragment = new VideoPlayerFragment();
			fragment.setVideoData(vd);
			getFragmentManager().beginTransaction().add(R.id.container, fragment, VideoPlayerFragment.class.getName()).commit();
		}
		
	}
	
	@Override
    protected void onResume()
    {
    	super.onResume();
    	
    	if(!BluetoothCommunication.getInstance().isBluetoothEnabled())
    	{
    		finish();
    	}
    }
    
}

package com.immersion.wearablesdemo.activities;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.app.FragmentManager;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.immersion.wearablesdemo.R;
import com.immersion.wearablesdemo.controllers.BaseApplication;
import com.immersion.wearablesdemo.controllers.BluetoothCommunication;
import com.immersion.wearablesdemo.fragments.ScreenSlidePageFragment;
import com.immersion.wearablesdemo.models.VideoData;
import com.immersion.wearablesdemo.utils.FileUtils;
import com.immersion.wearablesdemo.utils.VideoDataHelper;

/**
 * Demonstrates a "screen-slide" animation using a {@link ViewPager}. Because {@link ViewPager}
 * automatically plays such an animation when calling {@link ViewPager#setCurrentItem(int)}, there
 * isn't any animation-specific code in this sample.
 *
 * <p>This sample shows a "next" button that advances the user to the next step in a wizard,
 * animating the current screen out (to the left) and the next screen in (from the right). The
 * reverse animation is played when the user presses the "previous" button.</p>
 *
 * @see ScreenSlidePageFragment
 */
public class ScreenSlideActivity extends FragmentActivity implements OnTouchListener {

    public static final int NUM_PAGES = 4;
     
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private int width = 0;
    private int height = 0;
    private ImageView cover;
    private ImageView solidCover;
	private boolean isSolidCoverVisible = false;
	private ImageView intro;
	private ImageView icons[];
	private TextView titles[];
	private int CurrentPage = 0;
	public boolean infoViewIsVisible = true;
	private Bitmap whites[];
	private Bitmap oranges[];
    private ImageView greys[];
    private ImageView fragmentCover;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // This should be the only call to this function.
        BluetoothCommunication.createBluetoothSingleton(this);
        
    	ViewGroup rootView;
    	
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
    	
        CurrentPage = 0;
        //videoInfo = new VideoData(this, CurrentPage,);
        setContentView(R.layout.activity_screen_slide);
        intro = (ImageView) this.findViewById(R.id.instructionView);
        cover = (ImageView) this.findViewById(R.id.imagecover);
		solidCover = (ImageView) this.findViewById(R.id.solidImagecover);
		fragmentCover = (ImageView) this.findViewById(R.id.fragmentCover);
		solidCover.setAlpha(0f);
        ImageView infoButton = (ImageView) this.findViewById(R.id.info);
        icons = new ImageView[4];
        whites =  new Bitmap[4];
        oranges = new Bitmap[4];
        greys = new ImageView[4];
        
        // TODO: Move all this initialization into a background task
        FileUtils.transferAllAssetMoviesToExternalStorage(this);
        
        for (int i = 0;i < 4;i++)
        {
        	final List<VideoData> videoData = VideoDataHelper.getPageVideoData(this, i);
        	
        	for(final VideoData vd : videoData)
        	{
                LinearLayout iconHolder = (LinearLayout) this.findViewById(R.id.iconLayout);
                
                RelativeLayout relativeLayout = new RelativeLayout(this);
                RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                iconHolder.addView(relativeLayout);

                greys[i] = new ImageView(this);
                greys[i].setImageResource(R.drawable.lightgrey);
                greys[i].setVisibility(View.INVISIBLE);
                greys[i].setScaleType(ScaleType.FIT_XY);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(480, 263);
                greys[i].setLayoutParams(layoutParams);
                relativeLayout.addView(greys[i]);
                
                //w=480 h=263
                icons[i] = new ImageView(this);
                icons[i].setVisibility(View.VISIBLE);
                whites[i] = vd.getWhiteIconForPage();
                oranges[i] = vd.getIconForPage();
                if (i == 0){
                	icons[i].setImageBitmap(oranges[i]);
                } else {
                	icons[i].setImageBitmap(whites[i]);
                }
                icons[i].setLongClickable(true);
                icons[i].setOnTouchListener(this);
                relativeLayout.addView(icons[i]);
                
                Log.d("debug", "width = "+ oranges[i].getWidth()+" height = "+oranges[i].getHeight());
                
                break;
 
        	}
        }	        
        
        
        infoButton.setOnTouchListener(new OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {

    	    	if (infoViewIsVisible == true){
    	    		return false;
    	    	}
            	
            	makeIntroVisible();
				return false;
            }
       });
        
        intro.setOnTouchListener(new OnTouchListener()
        {

            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
            	if (infoViewIsVisible == false){
            		return false;
            	}
            	
            	Animation fadeOut = new AlphaAnimation(1, 0);
                fadeOut.setInterpolator(new AccelerateInterpolator()); 
                fadeOut.setDuration(500);
            	
            	AnimationSet animation = new AnimationSet(false); 
                animation.addAnimation(fadeOut);
                animation.setRepeatCount(1);
                animation.setFillAfter(true);
                
                AnimationListener animListener = new AnimationListener(){

                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                        }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                    	intro.setVisibility(View.INVISIBLE);
                    	infoViewIsVisible = false;
                    }
                };
                animation.setAnimationListener(animListener);
                intro.startAnimation(animation);
            	
                return false;
            }
       });
        
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
        Log.d("screen size", "width="+width+" height="+height);
        
    }
    
    @Override
    protected void onResume()
    {
    	super.onResume();
//    	mPager.setCurrentItem(0);


        // Instantiate a ViewPager and a PagerAdapter.
    	mPager = null;
    	mPagerAdapter = null;
    	System.gc();
    	CurrentPage = 0;
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(final int position) {

                invalidateOptionsMenu();
                final int lastLocation = CurrentPage;
                int iconSize = width/4;
        		TranslateAnimation anim2 = new TranslateAnimation( iconSize*(CurrentPage), iconSize*(position) , 0, 0 );
        	    anim2.setDuration(200);
        	    anim2.setStartOffset(0);
        	    anim2.setFillAfter( true );
        	    AnimationListener animListener = new AnimationListener(){

                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                        }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        icons[lastLocation].setImageBitmap( whites[lastLocation]);
                        icons[position].setImageBitmap( oranges[position]);
                    	if (isSolidCoverVisible == true){
                    		removeSolidCover();
                    		showFragment();
                    		isSolidCoverVisible = false;
                    	}
                    	
                    }
                };
                anim2.setAnimationListener(animListener);
        	    cover.startAnimation(anim2);

                ScreenSlideActivity.this.CurrentPage = position;
            }
        });
    	
    	final BluetoothCommunication btc = BluetoothCommunication.getInstance();
    	
    	if(!btc.isBluetoothEnabled())
    	{
    		final Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
    	    startActivityForResult(intent, BluetoothCommunication.REQUEST_ENABLE_BT);
    	}
    	else
    	{
    		if(!btc.isConnecting() && !btc.isConnected())
    		{
    			btc.startAcceptThread();
    		}
    	}
    }
   
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            //return ScreenSlidePageFragment.create(position);
        	return ScreenSlidePageFragment.create(position);
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    public void makeIntroVisible(){
    	Log.d("debug", "make visible");
    	Animation fadeIn = new AlphaAnimation(0, 1);
    	fadeIn.setInterpolator(new AccelerateInterpolator()); 
    	fadeIn.setDuration(500);
    	
    	AnimationSet animation = new AnimationSet(false); 
        animation.addAnimation(fadeIn);
        animation.setRepeatCount(1);
        animation.setFillAfter(true);
        
        AnimationListener animListener = new AnimationListener(){

            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                }

            @Override
            public void onAnimationEnd(Animation animation) {
            	intro.setVisibility(View.VISIBLE);
        		infoViewIsVisible = true;
            }
        };
        animation.setAnimationListener(animListener);
        intro.startAnimation(animation);
    }
    
    public void removeSolidCover(){
    	ObjectAnimator anim = ObjectAnimator.ofFloat(solidCover, "alpha", 0.0f);
    	
    	anim.setDuration(200).start();
    }
    
    public void hideFragment(){
    		ObjectAnimator.ofFloat(fragmentCover, "alpha", 1.0f).setDuration(200).start();
    }
    
    public void showFragment(){
    	ObjectAnimator.ofFloat(fragmentCover, "alpha", 0.0f).setDuration(200).start();
    }
    
    public void fadeSolidColorAndSwitchToPage(final int pageNumber){
    	
    	ObjectAnimator anim = ObjectAnimator.ofFloat(solidCover, "alpha", 1.0f);
    	anim.addListener(new AnimatorListener() {
    	    
    	    @Override 
    	    public void onAnimationEnd(Animator animation) {
    	        Log.d("debug", "anim done");    
    	        isSolidCoverVisible = true;
    			mPager.setCurrentItem(pageNumber);     
    	    }

			@Override
			public void onAnimationCancel(Animator animation) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationRepeat(Animator animation) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationStart(Animator animation) {
				// TODO Auto-generated method stub
				
			}
    	    
    	});
    	anim.setDuration(200).start();
    	

    }
    
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		int i;
		
		
		for (i = 0;i < 4;i++){
			if (icons[i] == v){
				break;
			}
		}
		
		if (event.getAction() == event.ACTION_DOWN){
			greys[i].setVisibility(View.VISIBLE);
		}

		if (event.getAction() != event.ACTION_UP){
			return false;
		}
		greys[i].setVisibility(View.INVISIBLE);
		
		if (i == CurrentPage){//nothing to do 
			return false;
		}
		
		if (!(i+1 == CurrentPage || i-1 == CurrentPage)) { //we are NOT choose a next door page
			Log.d("debug", "fade out and set fade in bool");
			fadeSolidColorAndSwitchToPage(i);
            icons[CurrentPage].setImageBitmap( whites[CurrentPage]);
			hideFragment();
			return false;
		}
		
		mPager.setCurrentItem(i);
        return false;
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		
		// This should be the only call to this function.
		FileUtils.closeCloseable(BluetoothCommunication.getInstance());
	}
}

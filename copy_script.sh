#!/bin/sh

echo "----- Wearables copy shell script starting -----"

cp -r .settings ../WearablesDemo
cp -r assets ../WearablesDemo
cp -r libs ../WearablesDemo
cp -r res ../WearablesDemo
cp -r src ../WearablesDemo
cp .classpath ../WearablesDemo
cp .gitignore ../WearablesDemo
cp .project ../WearablesDemo
cp AndroidManifest.xml ../WearablesDemo
cp proguard-project.txt ../WearablesDemo
cp project.properties ../WearablesDemo

echo "----- Wearables copy shell script done -----"